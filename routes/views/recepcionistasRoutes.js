'use strict'
// const path = __dirname + '../../public/'
const path = require('path');
const authGuard = require('../../guard/auth')
const express = require('express')
const router = express.Router()

/* Rotas do sistema */
router.use(function (req, res, next) {
    console.log('VIEWS %s %s', req.method, req.url)
    next()
})

/* Dashboard do Sitema */
router.get('/', authGuard.isLoggedIn, authGuard.isAuthorized(['4']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','recepcionistas','dashboardRecepcionistas.html'))
})

router.get('/alterar-dados', authGuard.isLoggedIn, authGuard.isAuthorized(['4']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','recepcionistas','editarDados.html'))
})

router.get('/alterar-senha', authGuard.isLoggedIn, authGuard.isAuthorized(['4']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','recepcionistas','editarSenha.html'))
})

router.get('/agendamentos', authGuard.isLoggedIn, authGuard.isAuthorized(['4']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','recepcionistas','agendamentos.html'))
})

/*Cadastro/Listagem  de Pacientes*/
router.get('/cadastrar-paciente', authGuard.isLoggedIn, authGuard.isAuthorized(['4']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','recepcionistas','cadastroPacientes.html'))
})

router.get('/listar-pacientes', authGuard.isLoggedIn, authGuard.isAuthorized(['4']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','recepcionistas','listarPacientes.html'))
})

/*Cadastro/Listagem de Laboratorios*/
router.get('/cadastrar-laboratorio', authGuard.isLoggedIn, authGuard.isAuthorized(['4']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','recepcionistas','cadastroLaboratorios.html'))
})

router.get('/listar-laboratorios', authGuard.isLoggedIn, authGuard.isAuthorized(['4']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','recepcionistas','listarLaboratorios.html'))
})
module.exports = router
