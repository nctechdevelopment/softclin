'use strict'
// const path = __dirname + '../../public/'
const path = require('path');
const authGuard = require('../../guard/auth')
const express = require('express')
const router = express.Router()

/* Rotas do sistema */
router.use(function (req, res, next) {
    console.log('VIEWS %s %s', req.method, req.url)
    next()
})

/* Dashboard do Sitema */
router.get('/', authGuard.isLoggedIn, authGuard.isAuthorized(['2']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','laboratorios','dashboardLaboratorios.html'))
})

router.get('/alterar-dados', authGuard.isLoggedIn, authGuard.isAuthorized(['2']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','laboratorios','editarDados.html'))
})

router.get('/alterar-senha', authGuard.isLoggedIn, authGuard.isAuthorized(['2']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','laboratorios','editarSenha.html'))
})

router.get('/enviar-exames', authGuard.isLoggedIn, authGuard.isAuthorized(['2']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','laboratorios','enviarExames.html'))
})

module.exports = router
