'use strict'

// const path = __dirname + '../../public/'
const path = require('path');
const authGuard = require('../../guard/auth')
const express = require('express')
const router = express.Router()

/* Rotas do sistema */
router.use(function (req, res, next) {
    console.log('VIEWS %s %s', req.method, req.url)
    next()
})

/* Dashboard do Sitema */
router.get('/', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','adm','dashboard.html'))
})

/*Cadastro/Listagem de Laboratorios*/
router.get('/cadastrar-laboratorio', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','adm','cadastroLaboratorios.html'))
})

router.get('/listar-laboratorios', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','adm','listarLaboratorios.html'))
})

/*Cadastro/Listagem de médicos de Médicos*/
router.get('/cadastrar-medico', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','adm','cadastroMedicos.html'))
}) 

router.get('/listar-medicos', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','adm','listarMedicos.html'))
})

/*Cadastro/Listagem de Recepcionistas*/
router.get('/cadastrar-recepcionista', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','adm','cadastroRecepcionistas.html'))
})

router.get('/listar-recepcionistas', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','adm','listarRecepcionistas.html'))
})

/*Cadastro/Listagem  de Pacientes*/
router.get('/cadastrar-paciente', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','adm','cadastroPacientes.html'))
}) 

router.get('/listar-pacientes', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','adm','listarPacientes.html'))
})

/*Cadastro/Listagem de Medicamentos*/
router.get('/cadastrar-medicamento', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','adm','cadastroMedicamentos.html'))
})

router.get('/listar-medicamentos', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','adm','listarMedicamentos.html'))
})

/*Listagem  de Logins*/

router.get('/manutencao-logins', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','adm','manutencaoLogins.html'))
})

module.exports = router
