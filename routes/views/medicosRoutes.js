'use strict'
// const path = __dirname + '../../public/'
const path = require('path');
const authGuard = require('../../guard/auth')
const express = require('express')
const router = express.Router()

/* Rotas do sistema */
router.use(function (req, res, next) {
    console.log('VIEWS %s %s', req.method, req.url)
    next()
})

/* Dashboard do Sitema */
router.get('/', authGuard.isLoggedIn, authGuard.isAuthorized(['3']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','medicos','dashboardMedicos.html'))
})

router.get('/alterar-dados', authGuard.isLoggedIn, authGuard.isAuthorized(['3']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','medicos','editarDados.html'))
})

router.get('/alterar-senha', authGuard.isLoggedIn, authGuard.isAuthorized(['3']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','medicos','editarSenha.html'))
})

router.get('/minha-agenda', authGuard.isLoggedIn, authGuard.isAuthorized(['3']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','medicos','agenda.html'))
})

router.get('/historico-paciente', authGuard.isLoggedIn, authGuard.isAuthorized(['3']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','medicos','historicoPaciente.html'))
})

router.get('/resultado-exames', authGuard.isLoggedIn, authGuard.isAuthorized(['3']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','medicos','downloadExames.html'))
})

router.get('/atendimento?:id', authGuard.isLoggedIn, authGuard.isAuthorized(['3']), function (req, res) {
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','medicos','atendimento.html'))
})

/*Cadastro/Listagem de Laboratorios*/
router.get('/cadastrar-laboratorio', authGuard.isLoggedIn, authGuard.isAuthorized(['3']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','medicos','cadastroLaboratorios.html'))
})

router.get('/listar-laboratorios', authGuard.isLoggedIn, authGuard.isAuthorized(['3']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','medicos','listarLaboratorios.html'))
})

/*Cadastro/Listagem de Recepcionistas*/
router.get('/cadastrar-recepcionista', authGuard.isLoggedIn, authGuard.isAuthorized(['3']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','medicos','cadastroRecepcionistas.html'))
})

router.get('/listar-recepcionistas', authGuard.isLoggedIn, authGuard.isAuthorized(['3']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','medicos','listarRecepcionistas.html'))
})

/*Cadastro/Listagem  de Pacientes*/
router.get('/cadastrar-paciente', authGuard.isLoggedIn, authGuard.isAuthorized(['3']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','medicos','cadastroPacientes.html'))
}) 

router.get('/listar-pacientes', authGuard.isLoggedIn, authGuard.isAuthorized(['3']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','medicos','listarPacientes.html'))
})

/*Cadastro/Listagem de Medicamentos*/
router.get('/cadastrar-medicamento', authGuard.isLoggedIn, authGuard.isAuthorized(['3']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','medicos','cadastroMedicamentos.html'))
})

router.get('/listar-medicamentos', authGuard.isLoggedIn, authGuard.isAuthorized(['3']), function (req, res){
    res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'views','medicos','listarMedicamentos.html'))
})


module.exports = router
