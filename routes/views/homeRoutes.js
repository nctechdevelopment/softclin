'use strict'

// const path = __dirname + '../../public/'
const path = require('path');
const express = require('express')
const router = express.Router()
const authGuard = require('../../guard/auth')

/* Rotas do sistema */
router.use(function (req, res, next) {
    console.log('VIEWS %s %s', req.method, req.url)
    next()
})

router.route('/')
    .get(function (req, res) {
        res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'index.html'))
    })

/* Página de Login e rota de chamada do login */
router.route('/login')
    .get(function (req, res) {
        res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'login.html'))
    })

router.route('/resultado-exame')
    .get(function (req, res) {
        res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'loginExame.html'))
    })

router.get('/acesso-exames?:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1', '5']), function (req, res) {
        res.sendFile(path.resolve(__dirname, '..', '..', 'public', 'downloadExame.html'))
    })



module.exports = router
