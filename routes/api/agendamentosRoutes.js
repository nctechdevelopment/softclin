'use strict'
const authGuard = require('../../guard/auth')
const express = require('express')
const router = express.Router()
const AgendamentosControler = require('../../controller/AgendamentosController')

router.use(function (req, res, next) {
    console.log('API %s %s', req.method, req.url)
    next()
})

router.post('/', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3','4']), function (req, res) {
    AgendamentosControler.cadastrar(req, res)
})

router.get('/listarPorMedico:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3','4']), function (req, res){
    let id = req.params.id
    AgendamentosControler.listarPorMedico(id,res)
})

router.get('/:id_agend/:id_med', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res){
    let id_agend = req.params.id_agend
    let id_med = req.params.id_med
    AgendamentosControler.retornarAgendamento(id_agend, id_med,res)
})

router.put('/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3','4']), function (req, res) {
    let id = req.params.id;
    AgendamentosControler.editar(id, req, res)
})

router.delete('/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3','4']), function (req, res) {
    let id = req.params.id;
    AgendamentosControler.excluir(id, res)
})

router.post('/mudarStatus/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res) {
    let id = req.params.id;
    AgendamentosControler.mudarStatus(id, req, res)
})


module.exports = router