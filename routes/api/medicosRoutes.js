'use strict'

const authGuard = require('../../guard/auth')
const express = require('express')
const router = express.Router()
const MedController = require('../../controller/MedicosController')

router.use(function (req, res, next) {
    console.log('API %s %s', req.method, req.url)
    next()
})

router.post('/', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res) {
    MedController.cadastrar(req, res)
})

router.get('/', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res){
    MedController.listar(res)
})

router.get('/medicosDisponiveis', authGuard.isLoggedIn, authGuard.isAuthorized(['1','4']), function (req, res){
    MedController.medicosDisponiveis(res)
})

router.get('/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res){
    let id = req.params.id
    MedController.listarPorId(id,res)
})

router.put('/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res) {
    let id = req.params.id;
    MedController.editar(id, req, res)
})

router.post('/mudarStatus/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res) {
    let id = req.params.id;
    MedController.mudarStatus(id, req, res)
})

module.exports = router