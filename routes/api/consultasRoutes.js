'use strict'
const authGuard = require('../../guard/auth')
const express = require('express')
const router = express.Router()
const ConsultasControler = require('../../controller/ConsultasController')

router.use(function (req, res, next) {
    console.log('API %s %s', req.method, req.url)
    next()
})

router.post('/', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res) {
    ConsultasControler.cadastrar(req, res)
})

router.get('/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res){
    let id = req.params.id
    ConsultasControler.listarPorId(id,res)
})

router.get('/ultimas/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res){
    let id = req.params.id
    ConsultasControler.ultimas(id,res)
})

router.get('/historico/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res){
    let id = req.params.id
    ConsultasControler.historico(id,res)
})

module.exports = router