'use strict'

const authGuard = require('../../guard/auth')
const express = require('express')
const router = express.Router()
const PacientesControler = require('../../controller/PacientesController')

router.use(function (req, res, next) {
    console.log('API %s %s', req.method, req.url)
    next()
})

router.post('/', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3','4']), function (req, res) {
    PacientesControler.cadastrar(req, res)
})

router.get('/', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3','4']), function (req, res){
    PacientesControler.listar(res)
})

router.get('/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3','4']), function (req, res){
    let id = req.params.id
    PacientesControler.listarPorId(id,res)
})

router.put('/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3','4']), function (req, res) {
    let id = req.params.id;
    PacientesControler.editar(id, req, res)
})

router.get('/agendamento/:pesquisa', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3','4']), function (req, res) {
    let pesquisa = req.params.pesquisa;
    PacientesControler.pesquisar(pesquisa, res)
})

router.post('/mudarStatus/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3','4']), function (req, res) {
    let id = req.params.id;
    PacientesControler.mudarStatus(id, req, res)
})

module.exports = router