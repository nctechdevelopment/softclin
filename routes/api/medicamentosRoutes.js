'use strict'
const authGuard = require('../../guard/auth')
const express = require('express')
const router = express.Router()
const MedicamentosControler = require('../../controller/MedicamentosController')

router.use(function (req, res, next) {
    console.log('API %s %s', req.method, req.url)
    next()
})

router.post('/', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res) {
    MedicamentosControler.cadastrar(req, res)
})

router.get('/', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res){
    MedicamentosControler.listar(res)
})

router.get('/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res){
    let id = req.params.id
    MedicamentosControler.listarPorId(id,res)
})

router.get('/receita/:pesquisa', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res) {
    let pesquisa = req.params.pesquisa;
    MedicamentosControler.pesquisar(pesquisa, res)
})

router.put('/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res) {
    let id = req.params.id;
    MedicamentosControler.editar(id, req, res)
})

module.exports = router