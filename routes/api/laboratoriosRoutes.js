'use strict'

const authGuard = require('../../guard/auth')
const express = require('express')
const router = express.Router()
const LabController = require('../../controller/LaboratiosController')

router.use(function (req, res, next) {
    console.log('API %s %s', req.method, req.url)
    next()
})

router.post('/', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3','4']), function (req, res) {
    LabController.cadastrar(req, res)
})

router.get('/', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3','4']), function (req, res){
    LabController.listar(res)
})

router.get('/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','2','3','4']), function (req, res){
    let id = req.params.id
    LabController.listarPorId(id,res)
})

router.put('/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','2','3','4']), function (req, res) {
    let id = req.params.id;
    LabController.editar(id, req, res)
})

router.post('/mudarStatus/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3','4']), function (req, res) {
    let id = req.params.id;
    LabController.mudarStatus(id, req, res)
})
module.exports = router