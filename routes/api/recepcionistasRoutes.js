'use strict'

const authGuard = require('../../guard/auth')
const express = require('express')
const router = express.Router()
const RecepController = require('../../controller/RecepcionistasController')

router.use(function (req, res, next) {
    console.log('API %s %s', req.method, req.url)
    next()
})

router.post('/', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res) {
    RecepController.cadastrar(req, res)
})

router.get('/', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res){
    RecepController.listar(res)
})

router.get('/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3','4']), function (req, res){
    let id = req.params.id
    RecepController.listarPorId(id,res)
})

router.put('/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3','4']), function (req, res) {
    let id = req.params.id;
    RecepController.editar(id, req, res)
})

router.post('/mudarStatus/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res) {
    let id = req.params.id;
    RecepController.mudarStatus(id, req, res)
})


module.exports = router