'use strict'

const path = __dirname + '/public/'
const authGuard = require('../../guard/auth')
const express = require('express')
const router = express.Router()
const passport = require('passport')
const HomeController = require('../../controller/HomeController')

router.use(function (req, res, next) {
    console.log('API %s %s', req.method, req.url)
    next()
})

router.get('/', authGuard.isLoggedIn, function (req, res) {
    if (req.session.passport.user.nivel_acesso == '1')
        res.redirect('/administrador')
    if (req.session.passport.user.nivel_acesso == '2')
        res.redirect('/laboratorios')
    if (req.session.passport.user.nivel_acesso == '3')
        res.redirect('/medicos')
    if (req.session.passport.user.nivel_acesso == '4')
        res.redirect('/recepcionistas')

})

router.get('/logins', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res) {
    HomeController.listar(res)
})

router.get('/logins/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res) {
    let id = req.params.id
    HomeController.listarPorId(id, res)
})

router.put('/logins/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1']), function (req, res) {
    let id = req.params.id;
    HomeController.editar(id, req, res)
})

router.put('/senhas/:id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','2','3','4']), function (req, res) {
    let id = req.params.id;
    HomeController.editarSenha(id, req, res)
})

router.route('/login')
    .post(function (req, res, next) {
        passport.authenticate('local-login',
            function (error, user, info) {
                if (error) return res.status(500).json({ info: 'Desculpe-nos! Tente novamente.' })
                if (!user) return res.status(403).json({ info })
                req.login(user, function (error) {
                    if (error) return res.status(500).json({ info: 'Desculpe-nos! Tente novamente.' })
                    return res.status(200).json({ user, info })
                })
            }
        )(req, res, next)
    })

router.route('/loginExame')
    .post(function (req, res, next) {
        passport.authenticate('local-login2',
            function (error, user, info) {
                if (error) return res.status(500).json({ info: 'Desculpe-nos! Tente novamente.' })
                if (!user) return res.status(403).json({ info })
                req.login(user, function (error) {
                    if (error) return res.status(500).json({ info: 'Desculpe-nos! Tente novamente.' })
                    return res.status(200).json({ user, info })
                })
            }
        )(req, res, next)
    })

router.get('/logout', function (req, res) {
    req.session.destroy(function (error) {
        if (error) { return next(error) }
        res.clearCookie('SoftClin')
        req.logout()
        res.redirect('/')
    })
})

module.exports = router