'use strict'
const authGuard = require('../../guard/auth')
const express = require('express')
const router = express.Router()
const ExamesControler = require('../../controller/ExamesControler')
const multer = require('multer')
const cuid = require('cuid')

const storage = multer.diskStorage({ 
    destination: function (req, file, cb) {
        cb(null, 'public/exames/uploads/')
    },
    filename: function (req, file, cb) {
        cb(null, cuid() + '.pdf');
    }
});

const upload = multer({ 
    storage : storage,
    fileFilter: function (req, file, cb) {
        if (file.mimetype !== 'application/pdf') {
            return cb(null, false, new Error('Arquivo não permitido'));
        }
        cb(null, true);
      }
 });

router.use(function (req, res, next) {
    console.log('API %s %s', req.method, req.url)
    next()
})

router.post('/', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res) {
    ExamesControler.cadastrar(req, res)
})

router.get('/tipo/:pesquisa', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res) {
    let pesquisa = req.params.pesquisa;
    ExamesControler.pesquisar(pesquisa, res)
})

router.get('/examesConsulta/:consulta_id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','2','3']), function (req, res){
    let id = req.params.consulta_id
    ExamesControler.examesConsulta(id,res)
})

router.get('/downloadExames/:id_consulta/:id_paciente', authGuard.isLoggedIn, authGuard.isAuthorized(['1','5']), function (req, res){
    let id_consulta = req.params.id_consulta
    let id_paciente = req.params.id_paciente
    ExamesControler.downloadExames(id_consulta, id_paciente, res)
})

router.get('/resultadoExames/:id_consulta', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res){
    let id_consulta = req.params.id_consulta
    ExamesControler.resultadoExames(id_consulta, res)
})

router.put('/enviarExames/:id_consulta', authGuard.isLoggedIn, authGuard.isAuthorized(['1','2','3']), upload.single('arqUpload'), function (req, res){
    let id = req.params.id_consulta;
    let path = "exames/uploads/" +  req.file.filename
    ExamesControler.enviarExames(id, path, req, res)
})

module.exports = router