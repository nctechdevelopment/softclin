'use strict'
const authGuard = require('../../guard/auth')
const express = require('express')
const router = express.Router()
const ReceitasControler = require('../../controller/ReceitasControler')

router.use(function (req, res, next) {
    console.log('API %s %s', req.method, req.url)
    next()
})

router.post('/', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res) {
    ReceitasControler.cadastrar(req, res)
})

router.get('/medicamentosReceita/:consulta_id', authGuard.isLoggedIn, authGuard.isAuthorized(['1','3']), function (req, res){
    let id = req.params.consulta_id
    ReceitasControler.medicamentosReceita(id,res)
})
module.exports = router