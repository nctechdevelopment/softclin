'use strict'

const mysql = require('mysql2'),
        pool = mysql.createPool({
        database: 'softclin',
        host: 'localhost',
        user: 'root',
        password: 'nctech',
    }),
    promisePool = pool.promise()
    
pool.getConnection(function(err) {
    if (err) {
        console.log('Error Connecting: ' + err)
    }
});

module.exports = promisePool
