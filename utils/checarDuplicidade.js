'use strict'

const connection = require('../database/connection')

async function checarDuplicidade(dados, campos, tabelas, chaves, id = null) {

    let msgErros = ''
    let concat = ''
    let [result] = ''

    

    for (let i = 0; i < campos.length; i++) {
        [result] = ''

        let sql = 'SELECT id FROM ' + tabelas[i] + ' WHERE ' + campos[i] + ' = ?';
        if (id) { concat = 'AND id != ?' }
        sql = sql + concat + ' LIMIT 1';

        try {

            
            if (id) {
                [result] = await connection.query(sql, [dados[campos[i]], id]);
            } else {
                [result] = await connection.query(sql, dados[campos[i]])
            }

            if (result.length) {
                msgErros = msgErros + chaves[i] + ',';
            }

        } catch (err) {

        }
    }

    return msgErros;
}

module.exports = { checarDuplicidade }