'use strict'

const bodyParser = require('body-parser'),
    express = require('express'),
    passport = require('passport'),
    session = require('express-session'),
    secret = require('./config/secret'),
    app = express()

app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(bodyParser.json())
app.use(express.static('public'))
app.use(session({
    name: 'SoftClin',
    secret: secret.key,
    cookie: {
        maxAge: 3600000
    },
    resave: false,
    saveUninitialized: false
}))
app.use(passport.initialize())
app.use(passport.session())

const port = process.env.PORT || 8080,
    hostname = 'localhost'
app.listen(port, onStart())

// Passport
require('./config/passport')(passport)

//Routes
const accessRoutes = require('./routes/api/homeRoutes');
const accessViews = require('./routes/views/homeRoutes');
const admViews = require('./routes/views/administradorRoutes');
const labRoutes = require('./routes/api/laboratoriosRoutes');
const labViews= require('./routes/views/laboratoriosRoutes');
const medRoutes = require('./routes/api/medicosRoutes');
const medViews = require('./routes/views/medicosRoutes');
const agdRoutes = require('./routes/api/agendamentosRoutes')
const consRoutes = require('./routes/api/consultasRoutes')
const receitasRoutes = require('./routes/api/receitasRoutes');
const examesRoutes = require('./routes/api/examesRoutes');
const recepViews= require('./routes/views/recepcionistasRoutes');
const recepRoutes = require('./routes/api/recepcionistasRoutes');
const pacieRoutes = require('./routes/api/pacientesRoutes');
const medicamentosRoutes = require('./routes/api/medicamentosRoutes');

//API Endpoints
app.use('/api/access', accessRoutes);
app.use('/', accessViews);
app.use('/administrador', admViews);
app.use('/laboratorios', labViews);
app.use('/api/laboratorios', labRoutes);
app.use('/medicos', medViews);
app.use('/api/medicos', medRoutes);
app.use('/api/agendamentos', agdRoutes);
app.use('/api/consultas', consRoutes);
app.use('/api/receitas', receitasRoutes);
app.use('/api/exames', examesRoutes);
app.use('/recepcionistas', recepViews);
app.use('/api/recepcionistas', recepRoutes);
app.use('/api/pacientes', pacieRoutes);
app.use('/api/medicamentos', medicamentosRoutes);

function onStart() {
    console.log(`Servidor iniciado em http://${hostname}:${port}`)
}
