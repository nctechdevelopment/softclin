'use strict'

const path = __dirname + '/public/'
const controller = require('./controller/controller')

module.exports = function (app, passport) {
    
    /*Rotas do sistema */
    app.use(function(req, res, next){
        console.log('%s %s', req.method, req.url)
        next()
    })

    /*Home do Sistema*/
    app.get('/', function (req, res) {
        res.sendFile(path + 'index.html')
    })

    /*Página de Login e rota de chamada do login*/
    app.route('/login')
        .get(function (req, res){
            res.sendFile(path + 'login.html')
        })
        .post(function (req, res, next){
            passport.authenticate('local-login',
                function (error, user, info) {
                    if (error) return res.status(500).json({ info: 'Desculpe-nos! Tente novamente.' })
                    if (!user) return res.status(403).json({ info })
                    req.login(user, function (error) {
                        if (error) return res.status(500).json({ info: 'Desculpe-nos! Tente novamente.'})
                        return res.status(200).json({ info })
                    })
                }
            )(req, res, next)
        })
    
    /*Redirecionando o acesso*/
    app.get('/access', isLoggedIn, function(req, res) {
        if (req.session.passport.user.nivel_acesso == '1')
            res.redirect('/administrador')
    })


    /*Home do Administrador*/
    app.get('/administrador', isLoggedIn, isAuthorized(['1']), function (req, res){
        res.sendFile(path + 'views/adm/dashboard.html')
        
    })

    /*Cadastro de Laboratorios*/
    app.route('/cadastrarLaboratorio')
        .get(isLoggedIn, isAuthorized(['1']), function (req, res){
            res.sendFile(path + 'views/adm/cadastroLaboratorios.html')
        })

    /*Logou*/
    app.get('/logout', function (req, res) {
        req.session.destroy(function (error){
            if(error) { return next(error)}
            res.clearCookie('SoftCin')
            req.logout()
            res.redirect('/')
        })
    })
}

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next()
    else
        res.redirect('/logout')
}

function isAuthorized(access) {
    return function (req, res, next) {
        if (access.indexOf(req.session.passport.user.nivel_acesso) != -1)
            return next()
        else
            res.redirect('/logout')
    }
}