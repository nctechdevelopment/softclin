var id_consulta = '';

jQuery(document).ready(function () {
    $('.form-exames').submit(function (event) {
        event.preventDefault()
        validacaoEdicao()
    })

})

function procurarPedido() {

    limparCampos()
    if ($("#id_consulta").val() == "") {
        toastr.error("Preencha o campo de ID!");
        limparCampos()
    } else {
        id_consulta = $("#id_consulta").val();
        $.ajax({
            url: '/api/exames/examesConsulta/' + id_consulta,
            type: 'get',
            dataType: 'json',
            async: true,
        }).done(function (callback) {
            if (callback.exames.length) {
                
                const exam = callback.exames.filter((exames) => {
                    return exames.status_entrega === '0'
                })

                if (exam.length) {
                    setarValores(exam)
                } else {
                    limparCampos();
                    toastr.success("Todos exames solicitados nesse ID já foram enviados!");
                }

            } else {
                limparCampos();
                toastr.error("Não existem pedidos de exames com esse ID!");
            }
        })
    }

}

function setarValores(exames) {

    for (i = 0; i < exames.length; i++) {
        $("#exame").append('<option value="' + exames[i].id_solicitacao + '">' + exames[i].nome_exame + '</option>')
    }

    $("#data_sol").val(moment(exames[0].data_solicitacao).format('YYYY-MM-DD'))
    $("#nome_medico").val(exames[0].nome_medico)
    $("#crm").val(exames[0].crm)
    $("#telefone").val(exames[0].telefone)
    $("#celular_medico").val(exames[0].celular_medico)
    $("#nome_paciente").val(exames[0].nome_paciente)
    $("#cpf").val(exames[0].cpf)
    $("#cns").val(exames[0].cns)
    $("#celular_paciente").val(exames[0].celular_paciente)

    $("#dados_exames1").css("display", "flex");
    $("#dados_exames2").css("display", "flex");
    $("#dados_exames3").css("display", "flex");
    $("#dados_exames4").css("display", "flex");
    $("#dados_exames5").css("display", "flex");
    $("#dados_exames6").css("display", "flex");
    $("#dados_exames7").css("display", "flex");
    $("#dados_exames8").css("display", "flex");
    $("#dados_exames9").css("display", "flex");
}

function limparCampos() {
    $("#data_sol").val('')
    $("#nome_medico").val('')
    $("#crm").val('')
    $("#telefone").val('')
    $("#celular_medico").val('')
    $("#nome_paciente").val('')
    $("#cpf").val('')
    $("#cns").val('')
    $("#celular_paciente").val('')
    $("#arqUpload").val('')
    $("#exame").empty();


    $("#dados_exames1").css("display", "none");
    $("#dados_exames2").css("display", "none");
    $("#dados_exames3").css("display", "none");
    $("#dados_exames4").css("display", "none");
    $("#dados_exames5").css("display", "none");
    $("#dados_exames6").css("display", "none");
    $("#dados_exames7").css("display", "none");
    $("#dados_exames8").css("display", "none");
    $("#dados_exames9").css("display", "none");
}

function validacaoEdicao() {
    if (id_consulta == '' ||
        $("#exame").val() == "" ||
        $("#arqUpload").val() == "") {
        toastr.error("Verifique se todos campos obrigatórios estão preenchidos!")
    } else if ($("#arqUpload")[0].files[0].type != "application/pdf") {
        toastr.error("São aceitos somente arquivos em PDF!")
    } else {
        enviarDados()
    }
}

function enviarDados() {

    var formData = new FormData($('form')[0])

    formData.append("dataAtual", moment().format('YYYY-MM-DD'))
    formData.append("nomeOriginal", $("#arqUpload")[0].files[0].name)
    formData.delete("id_consulta")

    $.ajax({
        url: '/api/exames/enviarExames/' + id_consulta,
        type: 'put',
        async: true,
        processData: false,
        contentType: false,
        data: formData,
    }).done(function (callback) {
        toastr.success(callback.message)
        $("#id_consulta").val(id_consulta);
        procurarPedido()
    }).fail(function (callback) {
        msgErro = JSON.parse(callback.responseText)
        toastr.error(msgErro)
    })
}