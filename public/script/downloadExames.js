var consulta_id = window.location.search.substring(1),
    paciente_id = localStorage.getItem('user_id')

jQuery(document).ready(function () { 
    
    $.ajax({
        url: '/api/exames/downloadExames/' + consulta_id + '/' + paciente_id,
        type: "get",
        dataType: "json",
        assync: true
    }).done(function (callback){
        if (callback.exames == 0) {
            toastr.options.onHidden = function () { window.location = "/api/access/logout" }
            toastr.options.timeOut = 1500;
            toastr.error("Você não tem acesso a estes exames!");
        } else {
            $('#name_pac').text( localStorage.getItem('nome') + " -  Seja Bem Vindo! ")
            criarTabela(callback.exames);
        }
    })
        
})

function criarTabela(exames){
    $("#tbodyDownloads tr").remove()
    if (exames.length) {
        for (i = 0; i < exames.length; i++) {
            newTrItem = $("<tr>" +
                "<td>" + exames[i].nome_exame + "</td>" +
                "<td>" + exames[i].data_entrega + "</td>" +
                "<td><a href='" + exames[i].path + "' download='" + exames[i].nome_original + "'>Baixar Arquivo</a></td>" +
                "</tr>")
            appendTable(newTrItem)
        }
    } else {
        newTrItem = $("<tr class='danger'><td colspan='6'>Nenhum resultado encontrado.</td></tr>")
        appendTable(newTrItem)
    }
}

function appendTable(newTrItem) {
    $("#tbodyDownloads").append(newTrItem)
}
