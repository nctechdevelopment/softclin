jQuery(document).ready(function () {
    $('.form-recepcionistas').submit(function (event) {
        event.preventDefault()
        validacaoEdicao()
    })

})

function validacaoEdicao() {
    let msgErrors = ""

    //Tratar campos vazios
    if ($('#senha_atual').val() == "" ||
        $('#senha').val() == "" ||
        $('#confirmar_senha').val() == "") {
        msgErrors = "Verifique se todos campos obrigatórios estão preenchidos!"
    }

    //Verificar se a senha está igual
    if ($('#senha').val() != $('#confirmar_senha').val()) {
        msgErrors = "Nova senha e confirmar nova senha estão diferentes!"
    }

    //Verificando se tem erro ou não
    if (msgErrors) {
        enviarMsg(msgErrors)
    } else {
        enviarDados();
    }
}

function enviarMsg(msg) {
    toastr.error(msg);
    return false;
}

function enviarDados() {

    let id = localStorage.getItem('id_login')

    $.ajax({
        url: '/api/access/senhas/' + id,
        type: 'put',
        dataType: 'json',
        async: true,
        data: $("form").serialize(),
    }).done(function (callback) {
        toastr.options.onHidden = function () { window.location.reload() }
        toastr.options.timeOut = 1;
        toastr.success(callback.message)
    }).fail(function (callback) {
        console.log(callback)
        msgErro = JSON.parse(callback.responseText)
        enviarMsg(msgErro.message)
    })

}