jQuery(document).ready(function (){
    $('.form-recepcionistas').submit(function (event){
        event.preventDefault()
        validacaoEdicao()
    })
    editarUsuario(localStorage.getItem('user_id'))
    
})

function editarUsuario(id){
    $.ajax({
        url: '/api/recepcionistas/' + id,
        type: 'get',
        dataType: 'json',
        async: true,
    }).done(function (callback) {
        id_edit = callback.recepcionista[0].id;
        $("#nome").val(callback.recepcionista[0].nome)
        $("#data_nascimento").val(callback.recepcionista[0].data_nascimento)
        $("#cpf").val(callback.recepcionista[0].cpf)
        $("#rg").val(callback.recepcionista[0].rg)
        $("#celular").val(callback.recepcionista[0].celular)
        $("#telefone").val(callback.recepcionista[0].telefone)
        $("#cep").val(callback.recepcionista[0].cep)
        $("#rua").val(callback.recepcionista[0].rua)
        $("#complemento").val(callback.recepcionista[0].complemento)
        $("#bairro").val(callback.recepcionista[0].bairro)
        $("#cidade").val(callback.recepcionista[0].cidade)
        $("#uf").val(callback.recepcionista[0].uf)
        $("#numero").val(callback.recepcionista[0].numero)
    })
}

function validacaoEdicao() {
    let msgErrors = ""
    
    //Tratar campos vazios
    if ($('#nome').val() == "" ||
        $('#data_nascimento').val() == "" ||
        $('#cpf').val() == "" ||
        $('#rg').val() == "" ||
        $('#celular').val() == "" ||
        $('#cep').val() == "" ||
        $('#rua').val() == "" ||
        $('#bairro').val() == "" ||
        $('#cidade').val() == "" ||
        $('#uf').val() == "" ||
        $('#numero').val() == ""  ){
        msgErrors = "Verifique se todos campos obrigatórios estão preenchidos!"
    }

    //Campos incorretos
    if ($('#telefone').val().length > 0) {
        if ($('#telefone').val().length !== 14) {
            msgErrors = "Preencha o telefone corretamente!"
        }
    }
    if ($('#celular').val().length > 0) {
        if ($('#celular').val().length !== 15) {
            msgErrors = "Preencha o celular corretamente!"
        }
    }
    if ($('#cpf').val().length > 0) {
        if ($('#cpf').val().length !== 14) {
            msgErrors = "Preencha o CPF corretamente!"
        }
    }
    if ($('#cep').val().length > 0) {
        if ($('#cep').val().length !== 9) {
            msgErrors = "Preencha o CEP corretamente!"
        }
    }

    //Verificando se tem erro ou não
    if (msgErrors) {
        enviarMsg(msgErrors)
    }else{
        enviarDados();
    }
}

function enviarMsg(msg) {
    toastr.error(msg);
    return false;
}

function enviarDados(){
    
    $.ajax({
        url: '/api/recepcionistas/' + id_edit,
        type: 'put',
        dataType: 'json',
        async: true,
        data: $("form").serialize(),
    }).done(function (callback) {
        localStorage.setItem('nome', $('#nome').val())
        toastr.options.onHidden = function() { window.location.reload() }
        toastr.options.timeOut = 1;
        toastr.success(callback.message)
    }).fail(function (callback) {
        msgErro = JSON.parse(callback.responseText)
        enviarMsg(msgErro.message)
    })

}