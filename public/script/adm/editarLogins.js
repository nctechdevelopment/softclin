jQuery(document).ready(function (){
    $('.form-logins').submit(function (event){
        event.preventDefault()
        validacaoEdicao()
    })
})

var id_edit =  "";

function editarUsuario(id){
    $.ajax({
        url: '/api/access/logins/' + id,
        type: 'get',
        dataType: 'json',
        async: true,
    }).done(function (callback) {
        id_edit = callback.login[0].id;
        $("#email").val(callback.login[0].email)
        $("#senha").val("")
        $("#confirmar_senha").val("")
    })
}

function validacaoEdicao() {
    msgErrors = ""

    //Tratar campos vazios
    if ($('#email').val() == "" ||
        $('#senha').val() == "" ||
        $('#confirmar_senha').val() == "" ){
        msgErrors = "Verifique se todos campos obrigatórios estão preenchidos!"
    }

    //Verificar se a senha está igual
    if ($('#senha').val() != $('#confirmar_senha').val()) {
        msgErrors = "Senhas estão diferentes!"
    }

    //Verificando se tem erro ou não
    if (msgErrors) {
        enviarMsg(msgErrors)
    }else{
        enviarDados()
    }
}

function enviarMsg(msg) {
    toastr.error(msg);
    return false;
}

function enviarDados(){
    
    $.ajax({
        url: '/api/access/logins/' + id_edit,
        type: 'put',
        dataType: 'json',
        async: true,
        data: $("form").serialize(),
    }).done(function (callback) {
        toastr.success(callback.message)
        $('#modal-logins').modal('hide');
        $('#table-logins').DataTable().ajax.reload(null, false);
    }).fail(function (callback) {
        msgErro = JSON.parse(callback.responseText)
        enviarMsg(msgErro.message)
    })

}