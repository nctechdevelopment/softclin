//Form submit
jQuery(document).ready(function () {
    $('.form-medicamentos').submit(function (event) {
        event.preventDefault()
        validacaoRegistro()
    })
})

function validacaoRegistro() {
    msgErrors = "";

    //Tratar campos vazios de menor
    if ($('#nome_generico').val() == "" ||
        $('#nome_fabrica').val() == "" ||
        $('#composicao').val() == "" ||
        $('#descricao').val() == "" ) {
        msgErrors = "Verifique se todos campos obrigatórios estão preenchidos!"
    }

    //Verificando se tem erro ou não
    if (msgErrors) {
        enviarMsg(msgErrors)
    } else {
        enviarDados();
    }
}

function enviarMsg(msg) {
    toastr.error(msg);
    return false;
}

function enviarDados() {
    $.ajax({
        url: '/api/medicamentos/',
        type: 'post',
        dataType: 'json',
        async: true,
        data: $("form").serialize(),
    }).done(function (callback) {
        toastr.success(callback.message)
        $("#form-medicamentos")[0].reset()
    }).fail(function (callback) {
        msgErro = JSON.parse(callback.responseText)
        enviarMsg(msgErro.message)
    })
}