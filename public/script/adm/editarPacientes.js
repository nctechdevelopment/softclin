jQuery(document).ready(function () {
    $('.form-pacientes').submit(function (event) {
        event.preventDefault()
        validacaoEdicao()
    })

    $('#data_nascimento').blur(function () {

        var data = new Date();
        var d = data.getDate();
        var m = data.getMonth() + 1;
        var y = data.getFullYear();
        var dataAtual = y + '-' + (m <= 9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);

        if ($('#data_nascimento').val().length == 10 && dataAtual >= $('#data_nascimento').val()) {

            var data2 = new Date($('#data_nascimento').val());

            var dataAtual = moment(data, 'DD/MM/YYYY')
            var dataNasc = moment(data2, 'DD/MM/YYYY')
            var diff = dataAtual.diff(dataNasc, 'years')

            if (diff < 18) {
                $("#responsavel_nome").css("display", "flex");
                $("#responsavel_cpf").css("display", "flex");
            } else {
                $("#responsavel_nome").css("display", "none");
                $("#responsavel_cpf").css("display", "none");
            }

        } else {
            enviarMsg("Preencha a data corretemente!");
            $("#responsavel_nome").css("display", "none");
            $("#responsavel_cpf").css("display", "none");
        }

    })
})

var id_edit = "";

function mudarEstado(id, estado) {

    var status = { "status": 0 };

    if (estado == 0) {
        status = { "status": 1 }
    }

    $.ajax({
        url: '/api/pacientes/mudarStatus/' + id,
        type: 'post',
        dataType: 'json',
        async: true,
        data: status
    }).done(function (callback) {
        toastr.success(callback.message)
        $('#table-pacientes').DataTable().ajax.reload(null, false);
    }).fail(function (callback) {
        callbackMsg = JSON.parse(callback.responseText)
        toastr.error(callbackMsg.message);
    })

}

function editarUsuario(id) {
    $.ajax({
        url: '/api/pacientes/' + id,
        type: 'get',
        dataType: 'json',
        async: true,
    }).done(function (callback) {
        id_edit = callback.paciente[0].id;

        var data = new Date();
        var data2 = new Date(callback.paciente[0].data_nascimento);

        var dataAtual = moment(data, 'DD/MM/YYYY')
        var dataNasc = moment(data2, 'DD/MM/YYYY')
        var diff = dataAtual.diff(dataNasc, 'years')

        if (diff < 18) {
            $("#responsavel_nome").css("display", "flex");
            $("#responsavel_cpf").css("display", "flex");
            $("#nome_resp").val(callback.paciente[0].nome_resp)
            $("#cpf_resp").val(callback.paciente[0].cpf_resp)
        } else {
            $("#responsavel_nome").css("display", "none");
            $("#responsavel_cpf").css("display", "none");
            $("#nome_resp").val("")
            $("#cpf_resp").val("")
        }


        $("#nome").val(callback.paciente[0].nome)
        $("#data_nascimento").val(callback.paciente[0].data_nascimento)
        $("#cpf").val(callback.paciente[0].cpf)
        $("#rg").val(callback.paciente[0].rg)
        $("#cns").val(callback.paciente[0].cns)
        $("#tipo_sanguineo").val(callback.paciente[0].tipo_sanguineo)
        $("#sexo").val(callback.paciente[0].sexo)
        $("#celular").val(callback.paciente[0].celular)
        $("#telefone").val(callback.paciente[0].telefone)
        $("#cep").val(callback.paciente[0].cep)
        $("#rua").val(callback.paciente[0].rua)
        $("#complemento").val(callback.paciente[0].complemento)
        $("#bairro").val(callback.paciente[0].bairro)
        $("#cidade").val(callback.paciente[0].cidade)
        $("#uf").val(callback.paciente[0].uf)
        $("#numero").val(callback.paciente[0].numero)
        $("#doencas").val(callback.paciente[0].doencas)
        $("#limitacao_fis").val(callback.paciente[0].limitacao_fis)
        $("#alergia").val(callback.paciente[0].alergia)

    })
}

function validacaoEdicao() {
    msgErrors = "";

    //Verificar se é maior de 16
    var data = new Date();
    var data2 = new Date($('#data_nascimento').val());

    var dataAtual = moment(data, 'DD/MM/YYYY')
    var dataNasc = moment(data2, 'DD/MM/YYYY')
    var diff = dataAtual.diff(dataNasc, 'years')

    //Tratar campos vazios de menor/maior e adicionar campos
    if (diff < 18) {

        //Tratar campos vazios de menor
        if ($('#nome').val() == "" ||
            $('#data_nascimento').val() == "" ||
            $('#nome_resp').val() == "" ||
            $('#cpf_resp').val() == "" ||
            $('#cns').val() == "" ||
            $('#tipo_sanguineo').val() == null ||
            $('#sexo').val() == null ||
            $('#celular').val() == "" ||
            $('#cep').val() == "" ||
            $('#rua').val() == "" ||
            $('#bairro').val() == "" ||
            $('#cidade').val() == "" ||
            $('#uf').val() == "" ||
            $('#numero').val() == "") {
            msgErrors = "Verifique se todos campos obrigatórios estão preenchidos!"
        }

        if ($('#cpf_resp').val().length > 0) {
            if ($('#cpf_resp').val().length !== 14) {
                msgErrors = "Preencha o CPF do responsavel corretamente!"
            }
        }

    } else {

        //Tratar campos vazios de maior
        if ($('#nome').val() == "" ||
            $('#data_nascimento').val() == "" ||
            $('#cpf').val() == "" ||
            $('#rg').val() == "" ||
            $('#cns').val() == "" ||
            $('#tipo_sanguineo').val() == null ||
            $('#sexo').val() == null ||
            $('#celular').val() == "" ||
            $('#cep').val() == "" ||
            $('#rua').val() == "" ||
            $('#bairro').val() == "" ||
            $('#cidade').val() == "" ||
            $('#uf').val() == "" ||
            $('#numero').val() == "") {
            msgErrors = "Verifique se todos campos obrigatórios estão preenchidos!"
        }
    }

    //Campos incorretos
    if ($('#telefone').val().length > 0) {
        if ($('#telefone').val().length !== 14) {
            msgErrors = "Preencha o telefone corretamente!"
        }
    }
    if ($('#celular').val().length > 0) {
        if ($('#celular').val().length !== 15) {
            msgErrors = "Preencha o celular corretamente!"
        }
    }
    if ($('#cpf').val().length > 0) {
        if ($('#cpf').val().length !== 14) {
            msgErrors = "Preencha o CPF corretamente!"
        }
    }
    if ($('#cep').val().length > 0) {
        if ($('#cep').val().length !== 9) {
            msgErrors = "Preencha o CEP corretamente!"
        }
    }

    //Verificando se tem erro ou não
    if (msgErrors) {
        enviarMsg(msgErrors)
    } else {
        enviarDados();
    }
}

function enviarMsg(msg) {
    toastr.error(msg);
    return false;
}

function enviarDados() {

    $.ajax({
        url: '/api/pacientes/' + id_edit,
        type: 'put',
        dataType: 'json',
        async: true,
        data: $("form").serialize(),
    }).done(function (callback) {
        toastr.success(callback.message)
        $('#modal-pacientes').modal('hide');
        $('#table-pacientes').DataTable().ajax.reload(null, false);
    }).fail(function (callback) {
        msgErro = JSON.parse(callback.responseText)
        enviarMsg(msgErro.message)
    })

}