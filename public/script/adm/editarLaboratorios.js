jQuery(document).ready(function (){
    $('.form-laboratorios').submit(function (event){
        event.preventDefault()
        validacaoEdicao()
    })
})

var id_edit =  "";

function mudarEstado(id,estado){

    var status = {"status": 0};

    if(estado == 0){
        status = {"status": 1}
    }

    $.ajax({
        url: '/api/laboratorios/mudarStatus/' + id,
        type: 'post',
        dataType: 'json',
        async: true,
        data: status
    }).done(function(callback){
        toastr.success(callback.message)
        $('#table-laboratorios').DataTable().ajax.reload(null, false);
    }).fail(function (callback){
        callbackMsg = JSON.parse(callback.responseText)
        toastr.error(callbackMsg.message);
    })

}

function editarUsuario(id){
    $.ajax({
        url: '/api/laboratorios/' + id,
        type: 'get',
        dataType: 'json',
        async: true,
    }).done(function (callback) {
        id_edit = callback.laboratorio[0].id;
        $("#nome_fantasia").val(callback.laboratorio[0].nome_fantasia)
        $("#cnpj").val(callback.laboratorio[0].cnpj)
        $("#razao_social").val(callback.laboratorio[0].razao_social)
        $("#celular").val(callback.laboratorio[0].celular)
        $("#telefone").val(callback.laboratorio[0].telefone)
        $("#cep").val(callback.laboratorio[0].cep)
        $("#rua").val(callback.laboratorio[0].rua)
        $("#bairro").val(callback.laboratorio[0].bairro)
        $("#cidade").val(callback.laboratorio[0].cidade)
        $("#complemento").val(callback.laboratorio[0].complemento)
        $("#uf").val(callback.laboratorio[0].uf)
        $("#numero").val(callback.laboratorio[0].numero)
    })
}

function validacaoEdicao() {
    msgErrors = ""

    //Tratar campos vazios
    if ($('#nome_fantasia').val() == "" ||
        $('#cnpj').val() == "" ||
        $('#razao_social').val() == "" ||
        $('#telefone').val() == "" ||
        $('#celular').val() == "" ||
        $('#cep').val() == "" ||
        $('#rua').val() == "" ||
        $('#bairro').val() == "" ||
        $('#cidade').val() == "" ||
        $('#uf').val() == "" ||
        $('#numero').val() == "" ) {
        msgErrors = "Verifique se todos campos obrigatórios estão preenchidos!"
    }

    //Campos incorretos
    if ($('#telefone').val().length > 0) {
        if ($('#telefone').val().length !== 14) {
            msgErrors = "Preencha o telefone corretamente!"
        }
    }
    if ($('#celular').val().length > 0) {
        if ($('#celular').val().length !== 15) {
            msgErrors = "Preencha o celular corretamente!"
        }
    }
    if ($('#cnpj').val().length > 0) {
        if ($('#cnpj').val().length !== 18) {
            msgErrors = "Preencha o CNPJ corretamente!"
        }
    }
    if ($('#cep').val().length > 0) {
        if ($('#cep').val().length !== 9) {
            msgErrors = "Preencha o CEP corretamente!"
        }
    }

    //Verificando se tem erro ou não
    if (msgErrors) {
        enviarMsg(msgErrors)
    } else {
        enviarDados();
    }
}

function enviarMsg(msg) {
    toastr.error(msg);
    return false;
}

function enviarDados(){
    
    $.ajax({
        url: '/api/laboratorios/' + id_edit,
        type: 'put',
        dataType: 'json',
        async: true,
        data: $("form").serialize(),
    }).done(function (callback) {
        toastr.success(callback.message)
        $('#modal-laboratorios').modal('hide');
        $('#table-laboratorios').DataTable().ajax.reload(null, false);
    }).fail(function (callback) {
        msgErro = JSON.parse(callback.responseText)
        enviarMsg(msgErro.message)
    })

}