//Form submit
jQuery(document).ready(function () {
    $('.form-pacientes').submit(function (event) {
        event.preventDefault()
        validacaoRegistro()
    })
    $('#data_nascimento').blur(function () {

        var data = new Date();
        var d = data.getDate();
        var m = data.getMonth() + 1;
        var y = data.getFullYear();
        var dataAtual = y + '-' + (m <= 9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);

        if ($('#data_nascimento').val().length == 10 && dataAtual >= $('#data_nascimento').val()) {

            var data2 = new Date($('#data_nascimento').val());

            var dataAtual = moment(data, 'DD/MM/YYYY')
            var dataNasc = moment(data2, 'DD/MM/YYYY')
            var diff = dataAtual.diff(dataNasc, 'years')

            if (diff < 18) {
                $("#responsavel").css("display", "flex");
            } else {
                $("#responsavel").css("display", "none");
            }

        } else {
            enviarMsg("Preencha a data corretemente!");
            $("#responsavel").css("display", "none");
        }

    })
})

function validacaoRegistro() {
    msgErrors = "";

    //Verificar se é maior de 16
    var data = new Date();
    var data2 = new Date($('#data_nascimento').val());

    var dataAtual = moment(data, 'DD/MM/YYYY')
    var dataNasc = moment(data2, 'DD/MM/YYYY')
    var diff = dataAtual.diff(dataNasc, 'years')

    //Tratar campos vazios de menor/maior e adicionar campos
    if (diff < 18) {

        //Tratar campos vazios de menor
        if ($('#nome').val() == "" ||
            $('#data_nascimento').val() == "" ||
            $('#nome_resp').val() == "" ||
            $('#cpf_resp').val() == "" ||
            $('#cns').val() == "" ||
            $('#tipo_sanguineo').val() == null ||
            $('#sexo').val() == null ||
            $('#celular').val() == "" ||
            $('#cep').val() == "" ||
            $('#rua').val() == "" ||
            $('#bairro').val() == "" ||
            $('#cidade').val() == "" ||
            $('#uf').val() == "" ||
            $('#numero').val() == "") {
            msgErrors = "Verifique se todos campos obrigatórios estão preenchidos!"
        }

        if ($('#cpf_resp').val().length > 0) {
            if ($('#cpf_resp').val().length !== 14) {
                msgErrors = "Preencha o CPF do responsavel corretamente!"
            }
        }

    } else {

        //Tratar campos vazios de maior
        if ($('#nome').val() == "" ||
            $('#data_nascimento').val() == "" ||
            $('#cpf').val() == "" ||
            $('#rg').val() == "" ||
            $('#cns').val() == "" ||
            $('#tipo_sanguineo').val() == null ||
            $('#sexo').val() == null ||
            $('#celular').val() == "" ||
            $('#cep').val() == "" ||
            $('#rua').val() == "" ||
            $('#bairro').val() == "" ||
            $('#cidade').val() == "" ||
            $('#uf').val() == "" ||
            $('#numero').val() == "") {
            msgErrors = "Verifique se todos campos obrigatórios estão preenchidos!"
        }
    }

    //Campos incorretos
    if ($('#telefone').val().length > 0) {
        if ($('#telefone').val().length !== 14) {
            msgErrors = "Preencha o telefone corretamente!"
        }
    }
    if ($('#celular').val().length > 0) {
        if ($('#celular').val().length !== 15) {
            msgErrors = "Preencha o celular corretamente!"
        }
    }
    if ($('#cpf').val().length > 0) {
        if ($('#cpf').val().length !== 14) {
            msgErrors = "Preencha o CPF corretamente!"
        }
    }
    if ($('#cep').val().length > 0) {
        if ($('#cep').val().length !== 9) {
            msgErrors = "Preencha o CEP corretamente!"
        }
    }

    //Verificando se tem erro ou não
    if (msgErrors) {
        enviarMsg(msgErrors)
    } else {
        enviarDados();
    }
}

function enviarMsg(msg) {
    toastr.error(msg);
    return false;
}

function enviarDados() {
    $.ajax({
        url: '/api/pacientes/',
        type: 'post',
        dataType: 'json',
        async: true,
        data: $("form").serialize(),
    }).done(function (callback) {
        toastr.success(callback.message)
        $("#form-pacientes")[0].reset()
    }).fail(function (callback) {
        msgErro = JSON.parse(callback.responseText)
        enviarMsg(msgErro.message)
    })
}