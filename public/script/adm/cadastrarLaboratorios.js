//Form submit
jQuery(document).ready(function () {
    $('.form-laboratorios').submit(function (event) {
        event.preventDefault()
        validacaoRegistro()
    })
})

function validacaoRegistro() {
    msgErrors = ""

    //Tratar campos vazios
    if ($('#nome_fantasia').val() == "" ||
        $('#cnpj').val() == "" ||
        $('#razao_social').val() == "" ||
        $('#telefone').val() == "" ||
        $('#celular').val() == "" ||
        $('#cep').val() == "" ||
        $('#rua').val() == "" ||
        $('#bairro').val() == "" ||
        $('#cidade').val() == "" ||
        $('#uf').val() == "" ||
        $('#numero').val() == "" ||
        $('#email').val() == "" ||
        $('#senha').val() == "" ||
        $('#confirma_senha').val() == "") {
        msgErrors = "Verifique se todos campos obrigatórios estão preenchidos!"
    }

    //Campos incorretos
    if ($('#telefone').val().length > 0) {
        if ($('#telefone').val().length !== 14) {
            msgErrors = "Preencha o telefone corretamente!"
        }
    }
    if ($('#celular').val().length > 0) {
        if ($('#celular').val().length !== 15) {
            msgErrors = "Preencha o celular corretamente!"
        }
    }
    if ($('#cnpj').val().length > 0) {
        if ($('#cnpj').val().length !== 18) {
            msgErrors = "Preencha o CNPJ corretamente!"
        }
    }
    if ($('#cep').val().length > 0) {
        if ($('#cep').val().length !== 9) {
            msgErrors = "Preencha o CEP corretamente!"
        }
    }

    //Verificar se a senha está igual
    if ($('#senha').val() != $('#confirma_senha').val()) {
        msgErrors = "Senhas estão diferentes!"
    }

    //Verificando se tem erro ou não
    if (msgErrors) {
        enviarMsg(msgErrors)
    } else {
        enviarDados();
    }
}

function enviarMsg(msg) {
    toastr.error(msg);
    return false;
}

function enviarDados() {
    $.ajax({
        url: '/api/laboratorios/',
        type: 'post',
        dataType: 'json',
        async: true,
        data: $("form").serialize(),
    }).done(function (callback) {
        toastr.success(callback.message)
        $("#form-laboratorios")[0].reset()
    }).fail(function (callback) {
        msgErro = JSON.parse(callback.responseText)
        enviarMsg(msgErro.message)
    })
}