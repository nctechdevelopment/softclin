jQuery(document).ready(function (){
    $('.form-medicos').submit(function (event){
        event.preventDefault()
        validacaoEdicao()
    })
})

var id_edit =  "";

function mudarEstado(id,estado){

    var status = {"status": 0};

    if(estado == 0){
        status = {"status": 1}
    }

    $.ajax({
        url: "/api/medicos/mudarStatus/" + id,
        type: "post",
        dataType: "json",
        async: true,
        data: status
    }).done(function(callback){
        toastr.success(callback.message)
        $('#table-medicos').DataTable().ajax.reload(null, false);
    }).fail(function (callback){
        callbackMsg = JSON.parse(callback.responseText)
        toastr.error(callbackMsg.message);
    })

}

function editarUsuario(id){
    $.ajax({
        url: '/api/medicos/' + id,
        type: 'get',
        dataType: 'json',
        async: true,
    }).done(function (callback) {
        id_edit = callback.medico[0].id;
        $("#nome").val(callback.medico[0].nome)
        $("#data_nascimento").val(callback.medico[0].data_nascimento)
        $("#cpf").val(callback.medico[0].cpf)
        $("#rg").val(callback.medico[0].rg)
        $("#crm").val(callback.medico[0].crm)
        $("#celular").val(callback.medico[0].celular)
        $("#telefone").val(callback.medico[0].telefone)
        $("#cep").val(callback.medico[0].cep)
        $("#rua").val(callback.medico[0].rua)
        $("#complemento").val(callback.medico[0].complemento)
        $("#bairro").val(callback.medico[0].bairro)
        $("#cidade").val(callback.medico[0].cidade)
        $("#uf").val(callback.medico[0].uf)
        $("#numero").val(callback.medico[0].numero)
    })
}

function validacaoEdicao() {
    msgErrors = ""

    //Tratar campos vazios
    if ($('#nome').val() == "" ||
        $('#data_nascimento').val() == "" ||
        $('#cpf').val() == "" ||
        $('#rg').val() == "" ||
        $('#crm').val() == "" ||
        $('#celular').val() == "" ||
        $('#cep').val() == "" ||
        $('#rua').val() == "" ||
        $('#bairro').val() == "" ||
        $('#cidade').val() == "" ||
        $('#uf').val() == "" ||
        $('#numero').val() == ""  ){
        msgErrors = "Verifique se todos campos obrigatórios estão preenchidos!"
    }

    //Campos incorretos
    if ($('#telefone').val().length > 0) {
        if ($('#telefone').val().length !== 14) {
            msgErrors = "Preencha o telefone corretamente!"
        }
    }
    if ($('#celular').val().length > 0) {
        if ($('#celular').val().length !== 15) {
            msgErrors = "Preencha o celular corretamente!"
        }
    }
    if ($('#cpf').val().length > 0) {
        if ($('#cpf').val().length !== 14) {
            msgErrors = "Preencha o CPF corretamente!"
        }
    }
    if ($('#cep').val().length > 0) {
        if ($('#cep').val().length !== 9) {
            msgErrors = "Preencha o CEP corretamente!"
        }
    }

    //Verificando se tem erro ou não
    if (msgErrors) {
        enviarMsg(msgErrors)
    }else{
        enviarDados()
    }
}

function enviarMsg(msg) {
    toastr.error(msg);
    return false;
}

function enviarDados(){
    
    $.ajax({
        url: '/api/medicos/' + id_edit,
        type: 'put',
        dataType: 'json',
        async: true,
        data: $("form").serialize(),
    }).done(function (callback) {
        toastr.success(callback.message)
        $('#modal-medicos').modal('hide');
        $('#table-medicos').DataTable().ajax.reload(null, false);
    }).fail(function (callback) {
        msgErro = JSON.parse(callback.responseText)
        enviarMsg(msgErro.message)
    })

}