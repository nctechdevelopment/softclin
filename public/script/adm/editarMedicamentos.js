jQuery(document).ready(function () {
    $('.form-medicamentos').submit(function (event) {
        event.preventDefault()
        validacaoEdicao()
    })

})

var id_edit = "";

function editarUsuario(id) {
    $.ajax({
        url: '/api/medicamentos/' + id,
        type: 'get',
        dataType: 'json',
        async: true,
    }).done(function (callback) {
        id_edit = callback.medicamento[0].id;
        $("#nome_generico").val(callback.medicamento[0].nome_generico)
        $("#fabricante").val(callback.medicamento[0].fabricante)
        $("#nome_fabrica").val(callback.medicamento[0].nome_fabrica)
        $("#composicao").val(callback.medicamento[0].composicao)
        $("#descricao").val(callback.medicamento[0].descricao)
    })
}

function validacaoEdicao() {
    msgErrors = "";

    //Tratar campos vazios de menor
    if ($('#nome_generico').val() == "" ||
        $('#nome_fabrica').val() == "" ||
        $('#composicao').val() == "" ||
        $('#descricao').val() == "") {
        msgErrors = "Verifique se todos campos obrigatórios estão preenchidos!"
    }

    //Verificando se tem erro ou não
    if (msgErrors) {
        enviarMsg(msgErrors)
    } else {
        enviarDados();
    }
}

function enviarMsg(msg) {
    toastr.error(msg);
    return false;
}

function enviarDados() {

    $.ajax({
        url: '/api/medicamentos/' + id_edit,
        type: 'put',
        dataType: 'json',
        async: true,
        data: $("form").serialize(),
    }).done(function (callback) {
        toastr.success(callback.message)
        $('#modal-medicamentos').modal('hide');
        $('#table-medicamentos').DataTable().ajax.reload(null, false);
    }).fail(function (callback) {
        msgErro = JSON.parse(callback.responseText)
        enviarMsg(msgErro.message)
    })

}