jQuery(document).ready(function () {
    console.log(paciente_id)

    $('#table-consultas').DataTable({
        responsive: true,
        autoWidth: false,
        ajax: {
            url: '/api/consultas/ultimas/' + paciente_id,
            type: 'GET',
            dataSrc: 'consultas'
        },
        columns: [
            { 'data': 'data' },
            { 'data': 'motivo' },
            {
                'data': 'id', render: (data, type, row) => {                      
                    return `
                    <div class="btn-group">
                        <button type="button" class="btn btn-secondary btn-sm" data-toggle='modal' data-target='#modal-pacientes' onclick='exibirConsulta("${data}")'">
                            <i class="fas fa-edit"></i>
                        </button>
                    </div>
                    `;
                }
            },

        ],
        language: {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "Exibir _MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            },
            "select": {
                "rows": {
                    "_": "Selecionado %d linhas",
                    "0": "Nenhuma linha selecionada",
                    "1": "Selecionado 1 linha"
                }
            }
        }
    })

})