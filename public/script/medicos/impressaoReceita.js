'use strict'

function imprimirReceita(id_consulta){
    $.ajax({
        url: '/api/receitas/medicamentosReceita/' + id_consulta,
        type: 'get',
        dataType: 'json',
        async: true,
    }).done(function (callback) {

        if(callback.medicamentos.length){
            bootbox.confirm({
                message: "Deseja uma Receita Controlada?",
                buttons: {
                    confirm: {
                        label: 'Sim',
                        className: 'btn-success',
                        type: 'button'
                    },
                    cancel: {
                        label: 'Não',
                        className: 'btn-primary',
                        type: 'button'
                    },
                },
                callback: function (result) {
                    if (result == true) {
                        criarReceita(callback.medicamentos, 1)
                    } else {
                        criarReceita(callback.medicamentos, 0)
                    }
                }
            })
        } else {
            toastr.error("Não foram solicitados medicamentos nessa consulta!")
        }
          
    })
}

function criarReceita(medicamentos, tipo) {

    var pdf = new jsPDF();
    var diaAtual = moment().format('DD/MM/YYYY')

    var data = medicamentos.map(function (element) {
        return {
            nome_fabrica: element.nome_fabrica, nome_generico: element.nome_generico, 
            composicao: element.composicao, descricao: element.descricao, 
            posologia: element.posologia
        }
    })

    if(tipo == 1) {
        pdf.text('Receituário de Controle Especial', 70, 20)
    } else {
        pdf.text('Receituário', 90, 20)
    }
    
    pdf.setLineWidth(0.5)
    pdf.line(20, 25, 190, 25) 

    pdf.text('Paciente: ' + medicamentos[0].nome_paciente, 20, 40)

    var linha = 60;

    for(let i = 0; i < data.length; i++){

        pdf.text(data[i].nome_fabrica, 20 , linha)
        pdf.text(data[i].nome_generico, 80, linha)
        pdf.text(data[i].descricao, 120, linha)
        linha = linha+5;
        
        pdf.text('Composição: ' + data[i].composicao, 20, linha)
        linha = linha+10;

        pdf.text(data[i].posologia, 20, linha)
        linha = linha+20;
    }

    pdf.text('Dr: ' + medicamentos[0].nome_medico + " / CRM: " + medicamentos[0].crm , 60, 230)

    pdf.text(medicamentos[0].rua + " , N: " + medicamentos[0].numero, 60, 240)
    pdf.text(medicamentos[0].bairro + ", " + medicamentos[0].cidade + " - " + medicamentos[0].uf, 60, 250)
    pdf.text(medicamentos[0].celular + " / " + medicamentos[0].telefone, 60, 260)


    pdf.setLineWidth(0.5)
    pdf.line(20, 270, 190, 270)
    pdf.text(diaAtual, 20, 280)
    pdf.text('Receita eletronica gerada por Softclin!', 90, 280)
    
    pdf.save('Receituario')
}