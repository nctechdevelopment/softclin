var id_consulta = ''

jQuery(document).ready(function () {

})

function pesquisarPaciente() {

    if ($('#paciente').val() == "") {
        toastr.error("Preencha o campo de pesquisa")
    } else {
        $('#table-pacientes').DataTable().clear().destroy()

        $('#table-pacientes').DataTable({
            responsive: true,
            autoWidth: false,
            ajax: {
                url: '/api/pacientes/agendamento/' + $('#paciente').val(),
                type: 'GET',
                dataSrc: 'pacientes'
            },
            columns: [
                { 'data': 'nome' },
                { 'data': 'cpf' },
                { 'data': 'cns' },
                {
                    'data': 'id', render: (data, type, row) => {
                        return `
                        <div class="btn-group">
                            <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal-pacientes" onclick="selecionarPaciente('${row.nome}','${data}')">
                                <i class="fas fa-edit"></i>
                            </button>
                        </div>
                        `;
                    }
                },

            ],
            language: {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "Exibir _MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                },
                "select": {
                    "rows": {
                        "_": "Selecionado %d linhas",
                        "0": "Nenhuma linha selecionada",
                        "1": "Selecionado 1 linha"
                    }
                }
            }
        })

        $('#modal-pacientes').modal('show')

    }

}

function selecionarPaciente( nome, id) {
    $('#nome_pac').val(nome)
    $('#table-consultas').DataTable().clear().destroy()

    $('#table-consultas').DataTable({
        responsive: true,
        autoWidth: false,
        ajax: {
            url: '/api/consultas/historico/' + id,
            type: 'GET',
            dataSrc: 'consultas'
        },
        columns: [
            { 'data': 'data' },
            { 'data': 'motivo_consulta' },
            {
                'data': 'id', render: (data, type, row) => {
                    return `
                    <div class="btn-group">
                        <button type="button" class="btn btn-secondary btn-sm" data-toggle='modal' data-target='#modal-consultas' onclick='exibirConsulta("${data}")'">
                            <i class="fas fa-eye"></i>
                        </button>
                    </div>
                    `;
                }
            },

        ],
        language: {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "Exibir _MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            },
            "select": {
                "rows": {
                    "_": "Selecionado %d linhas",
                    "0": "Nenhuma linha selecionada",
                    "1": "Selecionado 1 linha"
                }
            }
        }
    })

    $('#tabela').css("display", "block");
}

function exibirConsulta(id) {
    $.ajax({
        url: '/api/consultas/' + id,
        type: 'get',
        dataType: 'json',
        async: true,
    }).done(function (callback) {
        id_consulta = id;
        $("#nome_medi_e").val(callback.consulta[0].nome_medico)
        $("#nome_pac_e").val(callback.consulta[0].nome_paciente)
        $("#data_consulta").val(moment(callback.consulta[0].data).format('YYYY-MM-DD'))
        $("#peso_e").val(callback.consulta[0].peso)
        $("#altura_e").val(callback.consulta[0].altura)
        $("#temperatura_e").val(callback.consulta[0].temperatura)
        $("#pa_e").val(callback.consulta[0].pressao)
        $("#fc_e").val(callback.consulta[0].frequencia_card)
        $("#fr_e").val(callback.consulta[0].frequencia_resp)
        $("#tipo_consulta_e").val(callback.consulta[0].tipo_consulta)
        $("#motivo_consulta_e").val(callback.consulta[0].motivo_consulta)
        $("#descricao_e").val(callback.consulta[0].descricao)
        $("#observacoes_e").val(callback.consulta[0].observacoes)
    })
}