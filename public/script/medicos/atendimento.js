var agendamento_id = window.location.search.substring(1),
    paciente_id = '',
    consulta_id = '',
    medicamento_id = '',
    exame_id = '',
    senha = ''

jQuery(document).ready(function () {
    $.ajax({
        url: '/api/agendamentos/' + window.location.search.substring(1) + '/' + localStorage.getItem('user_id'),
        type: 'get',
        dataType: 'json',
        async: true,
    }).done(function (callback) {
        if (callback.agendamento == 0) {
            toastr.options.onHidden = function () { window.location = "/medicos/minha-agenda" }
            toastr.options.timeOut = 1500;
            toastr.error("Não é possivel acessar esse agendamento! Você será redirecionado!");
        } else {
            setarValores(callback.agendamento[0])
            criarTabela();
        }
    })

    $('.form-consultas').submit(function (event) {
        event.preventDefault()
        validacaoRegistro()
    })

    $('.form-receitas').submit(function (event) {
        event.preventDefault()
        validacaoRegistroReceitas()
    })

    $('.form-exames').submit(function (event) {
        event.preventDefault()
        validacaoRegistroExames()
    })


    
})

function setarValores(agendamento) {

    paciente_id = agendamento.id_paciente;
    $("#nome_medi").val(agendamento.nome_medico)
    $("#data").val(moment().format('YYYY-MM-DD'))
    $("#nome_pac").val(agendamento.nome_paciente)
    $("#idade").val(moment().diff(agendamento.data_nascimento, 'years') + ' Anos')
    $("#cns").val(agendamento.cns)
    $("#tipo_sanguineo").val(agendamento.tipo_sanguineo)
    $("#sexo").val(agendamento.sexo)
    $("#celular").val(agendamento.celular)
    $("#telefone").val(agendamento.telefone)
    $("#doencas").val(agendamento.doencas)
    $("#limitacao_fis").val(agendamento.limitacao_fis)
    $("#alergia").val(agendamento.alergia)
}

function criarTabela() {
    $('#table-consultas').DataTable({
        responsive: true,
        autoWidth: false,
        ajax: {
            url: '/api/consultas/ultimas/' + paciente_id,
            type: 'GET',
            dataSrc: 'consultas'
        },
        columns: [
            { 'data': 'data' },
            { 'data': 'motivo_consulta' },
            {
                'data': 'id', render: (data, type, row) => {
                    return `
                    <div class="btn-group">
                        <button type="button" class="btn btn-secondary btn-sm" data-toggle='modal' data-target='#modal-consultas' onclick='exibirConsulta("${data}")'">
                            <i class="fas fa-eye"></i>
                        </button>
                    </div>
                    `;
                }
            },

        ],
        language: {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "Exibir _MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            },
            "select": {
                "rows": {
                    "_": "Selecionado %d linhas",
                    "0": "Nenhuma linha selecionada",
                    "1": "Selecionado 1 linha"
                }
            }
        }
    })
}

function resetForm() {
    $(':text:not("[readonly],[disabled]")').val('');
    $('select:not("[readonly],[disabled]")').val('');
    $('textarea:not("[readonly],[disabled]")').val('');
}

function validacaoRegistro() {

    //Tratar campos vazios
    if (agendamento_id == "" ||
        paciente_id == "" ||
        $('#tipo_consulta').val() == null ||
        $('#motivo_consulta').val() == "" ||
        $('#descricao').val() == "" ||
        $('#data').val() == "") {
        toastr.error("Verifique se todos campos obrigatórios estão preenchidos!");
    } else {
        enviarDados()
    }

}

function validacaoRegistroReceitas() {
    //Tratar campos vazios
    if (consulta_id == "" ||
        medicamento_id == "" ||
        $('#posologia').val() == "" ) {
        toastr.error("Verifique se todos campos obrigatórios estão preenchidos!");
    } else {
        enviarDadosReceita()
    }
}

function validacaoRegistroExames() {
    //Tratar campos vazios
    if (consulta_id == "" ||
        exame_id == "" ||
        $('#descricao_ex').val() == "" ) {
        toastr.error("Verifique se todos campos obrigatórios estão preenchidos!");
    } else {
        enviarDadosExame()
    }
}

function enviarDados() {
    var dados = {}

    $('.form-consultas').serializeArray().map(function (x) { dados[x.name] = x.value; });

    dados.medico_id = localStorage.getItem('user_id');
    dados.paciente_id = paciente_id;
    dados.agendamento_id = agendamento_id;

    $.ajax({
        url: '/api/consultas/',
        type: 'post',
        dataType: 'json',
        async: true,
        data: dados,
    }).done(function (callback) {
        consulta_id = callback.resultId;
        toastr.success(callback.message)
        $('#modal-solicitacoes').modal('show');
    }).fail(function (callback) {
        msgErro = JSON.parse(callback.responseText)
        toastr.error(msgErro)
    })
}

function enviarDadosReceita() {
    var dados = {}

    $('.form-receitas').serializeArray().map(function (x) { dados[x.name] = x.value; });

    dados.consulta_id = consulta_id;
    dados.medicamento_id = medicamento_id;

    $.ajax({
        url: '/api/receitas/',
        type: 'post',
        dataType: 'json',
        async: true,
        data: dados,
    }).done(function (callback) {
        $("#btn-receita").css("display", "block");
        toastr.success(callback.message)
        limparModal();
    }).fail(function (callback) {
        msgErro = JSON.parse(callback.responseText)
        toastr.error(msgErro)
    })
}

function enviarDadosExame() {
    var dados = {}
    var ano = moment().format('YYYY')
    var dia = moment().format('DD')

    senha = "" + ano + consulta_id + dia + paciente_id + ""

    $('.form-exames').serializeArray().map(function (x) { dados[x.name] = x.value; });

    dados.consulta_id = consulta_id;
    dados.exame_id = exame_id;
    dados.senha = senha;

    $.ajax({
        url: '/api/exames/',
        type: 'post',
        dataType: 'json',
        async: true,
        data: dados,
    }).done(function (callback) {
        $("#btn-exame").css("display", "block");
        toastr.success(callback.message)
        limparModal();
    }).fail(function (callback) {
        msgErro = JSON.parse(callback.responseText)
        toastr.error(msgErro)
    })
}

function exibirConsulta(id) {
    $.ajax({
        url: '/api/consultas/' + id,
        type: 'get',
        dataType: 'json',
        async: true,
    }).done(function (callback) {
        $("#nome_medi_e").val(callback.consulta[0].nome_medico)
        $("#nome_pac_e").val(callback.consulta[0].nome_paciente)
        $("#data_consulta").val(moment(callback.consulta[0].data).format('YYYY-MM-DD'))
        $("#tipo_consulta_e").val(callback.consulta[0].tipo_consulta)
        $("#peso_e").val(callback.consulta[0].peso)
        $("#altura_e").val(callback.consulta[0].altura)
        $("#temperatura_e").val(callback.consulta[0].temperatura)
        $("#pa_e").val(callback.consulta[0].pressao)
        $("#fc_e").val(callback.consulta[0].frequencia_card)
        $("#fr_e").val(callback.consulta[0].frequencia_resp)
        $("#motivo_consulta_e").val(callback.consulta[0].motivo_consulta)
        $("#descricao_e").val(callback.consulta[0].descricao)
        $("#observacoes_e").val(callback.consulta[0].observacoes)
    })
}

function pesquisarMedicamento() {

    $('#table-medicamentos').DataTable().clear().destroy()

    if ($('#medicamento').val() == "") {
        toastr.error("Preencha o campo de pesquisa")

    } else {

        $('#table-medicamentos').DataTable({
            responsive: true,
            autoWidth: false,
            ajax: {
                url: '/api/medicamentos/receita/' + $('#medicamento').val(),
                type: 'GET',
                dataSrc: 'medicamentos'
            },
            columns: [
                { 'data': 'nome_fabrica' },
                { 'data': 'nome_generico' },
                { 'data': 'descricao' },
                {
                    'data': 'id', render: (data, type, row) => {
                        return `
                        <div class="btn-group">
                            <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal-medicamentos" onclick="selecionarMedicamento('${row.nome_generico}','${row.nome_fabrica}','${row.descricao}','${row.composicao}','${data}')">
                                <i class="fas fa-edit"></i>
                            </button>
                        </div>
                        `;
                    }
                },

            ],
            language: {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "Exibir _MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                },
                "select": {
                    "rows": {
                        "_": "Selecionado %d linhas",
                        "0": "Nenhuma linha selecionada",
                        "1": "Selecionado 1 linha"
                    }
                }
            }
        })

        $('#modal-medicamentos').modal('show')
    }
}

function pesquisarExame() {

    $('#table-exames').DataTable().clear().destroy()

    if ($('#exame').val() == "") {
        toastr.error("Preencha o campo de pesquisa")

    } else {

        $('#table-exames').DataTable({
            responsive: true,
            autoWidth: false,
            ajax: {
                url: '/api/exames/tipo/' + $('#exame').val(),
                type: 'GET',
                dataSrc: 'exames'
            },
            columns: [
                { 'data': 'nome' },
                {
                    'data': 'id', render: (data, type, row) => {
                        return `
                        <div class="btn-group">
                            <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal-pesqexames" onclick="selecionarExame('${row.nome}','${data}')">
                                <i class="fas fa-edit"></i>
                            </button>
                        </div>
                        `;
                    }
                },

            ],
            language: {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "Exibir _MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                },
                "select": {
                    "rows": {
                        "_": "Selecionado %d linhas",
                        "0": "Nenhuma linha selecionada",
                        "1": "Selecionado 1 linha"
                    }
                }
            }
        })

        $('#modal-pesqexames').modal('show')
    }
}

function selecionarMedicamento(nome_gen, nome_fab, descricao_med, composicao, id_medicamento) {

    medicamento_id = id_medicamento;

    $("#dados_medicamentos1").css("display", "flex");
    $("#dados_medicamentos2").css("display", "flex");
    $("#dados_medicamentos3").css("display", "flex");
    $('#medicamento').val(nome_fab)
    $('#nome_generico').val(nome_gen)
    $('#descricao_rec').val(descricao_med)
    $('#composicao').val(composicao)
    
}

function selecionarExame(nome, id_exame) {

    exame_id = id_exame;

    $("#dados_exames").css("display", "flex")
    $('#exame').val(nome)
    
}

function limparModal(){

    $('#modal-receitas').modal('hide')
    $('#modal-exames').modal('hide')
    $('#modal-atestado').modal('hide')

    medicamento_id = '';
    exame_id = '';

    $("#dados_medicamentos1").css("display", "none")
    $("#dados_medicamentos2").css("display", "none")
    $("#dados_medicamentos3").css("display", "none")
    $("#dados_exames").css("display", "none")
    
    $('#exame').val('')
    $('#descricao_ex').val('')

    $('#medicamento').val('')
    $('#nome_generico').val('')
    $('#descricao_rec').val('')
    $('#composicao').val('')
    $('#posologia').val('')
    
    $('#periodo').val('')
    $('#cid').val('')
}

function fecharModal() {
    bootbox.confirm({
        message: "Deseja finalizar a consulta?",
        buttons: {
            confirm: {
                label: 'Sim',
                className: 'btn-danger',
                type: 'button'
            },
            cancel: {
                label: 'Não',
                className: 'btn-default'
            },
        },
        callback: function (result) {
            if (result == true) {
                toastr.options.onHidden = function () { window.location = "/medicos/minha-agenda" }
                toastr.options.timeOut = 1000;
                toastr.success("Consulta finalizada! Você será redirecionado!");
                $('#modal-solicitacoes').modal('hide');
            }
        }
    })
}
