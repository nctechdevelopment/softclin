var eventos = [],
    calendar = '',
    id = '',
    id_ed = '';

jQuery(document).ready(function () {

    $('.form-agendamentos').submit(function (event) {
        event.preventDefault(0)
        validacaoRegistro(0)
    })

    $('.form-edicao-agendamentos').submit(function (event) {
        event.preventDefault()
        validacaoRegistro(1)
    })

    var calendarEl = document.getElementById('calendar');

    calendar = new FullCalendar.Calendar(calendarEl, {
        locale: 'pt-br',
        plugins: ['interaction', 'dayGrid', 'timeGrid'],
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'dayGridMonth,timeGridWeek,timeGridDay'
        },
        minTime: '08:00',
        maxTime: '19:00',
        hiddenDays: [0],
        defaultView: 'timeGridWeek',
        defaultDate: new Date(),
        navLinks: true, // can click day/week names to navigate views
        selectable: true,
        selectMirror: true,
        selectOverlap: false,
        eventOverlap: false,
        businessHours: [{
            daysOfWeek: [1, 2, 3, 4, 5], // Monday - Friday
            startTime: '08:00',
            endTime: '12:00',
        }, {
            daysOfWeek: [1, 2, 3, 4, 5], // Monday - Friday (if adding lunch hours)
            startTime: '13:00',
            endTime: '19:00',
        }, {
            daysOfWeek: [6], // Monday - Friday (if adding lunch hours)
            startTime: '08:00',
            endTime: '12:00',
        }],
        selectConstraint: "businessHours",
        eventConstraint: "businessHours",
        selectAllow: function (selectInfo) {
            return moment().diff(selectInfo.start) <= 0
        },
        eventAllow: function (selectInfo) {
            return moment().diff(selectInfo.start) <= 0
        },
        select: function (arg) {
            id = '';
            $('#data_agendamento_start').val(moment(arg.start).format('YYYY-MM-DDTHH:mm'))
            $('#data_agendamento_end').val(moment(arg.end).format('YYYY-MM-DDTHH:mm'))
            $('#paciente').val("")
            $('#cpf').val("")
            $('#cns').val("")
            $('#paciente_id').val("")
            $("#dados").css("display", "none");
            $('#modal-agendamentos').modal('show')

        },
        eventClick: function (info) {

            var data = moment().format('DD-MM-YYYY')
            var dataEvent = moment(info.event.start).format('DD-MM-YYYY')
            $("#btn-atendimento").css("display", "none");

            if (data == dataEvent && info.event.extendedProps.status == 0) {
                id = info.event.extendedProps.id_paciente;
                id_ed = info.event.id;
                $('#data_agendamento_start_e').val(moment(info.event.start).format('YYYY-MM-DDTHH:mm'))
                $('#data_agendamento_end_e').val(moment(info.event.end).format('YYYY-MM-DDTHH:mm'))
                $('#paciente_e').val(info.event.title)
                $('#cpf_e').val(info.event.extendedProps.cpf)
                $('#cns_e').val(info.event.extendedProps.cns)
                $('#paciente_id_e').val(info.event.extendedProps.id_paciente)
                $("#btn-atendimento").css("display", "block");
                $('#modal-edicao-agendamentos').modal('show')
                return;
            } else if (info.event.extendedProps.status == 1) {
                enviarMsg("Atendimento já realizado!")
            } else if (moment(info.event.start).isAfter(moment())) {
                id = info.event.extendedProps.id_paciente;
                id_ed = info.event.id;
                $('#data_agendamento_start_e').val(moment(info.event.start).format('YYYY-MM-DDTHH:mm'))
                $('#data_agendamento_end_e').val(moment(info.event.end).format('YYYY-MM-DDTHH:mm'))
                $('#paciente_e').val(info.event.title)
                $('#cpf_e').val(info.event.extendedProps.cpf)
                $('#cns_e').val(info.event.extendedProps.cns)
                $('#paciente_id_e').val(info.event.extendedProps.id_paciente)
                $('#modal-edicao-agendamentos').modal('show')
            } else {
                enviarMsg("Não é possivel alterar agendamentos antigos!")
            }

        },
        eventResize: function (info) {
            if (info.event.extendedProps.status == 0) {
                id = info.event.extendedProps.id_paciente;
                id_ed = info.event.id;
                $("#btn-atendimento").css("display", "none");
                $('#data_agendamento_start_e').val(moment(info.event.start).format('YYYY-MM-DDTHH:mm'))
                $('#data_agendamento_end_e').val(moment(info.event.end).format('YYYY-MM-DDTHH:mm'))
                $('#paciente_e').val(info.event.title)
                $('#cpf_e').val(info.event.extendedProps.cpf)
                $('#cns_e').val(info.event.extendedProps.cns)
                $('#paciente_id_e').val(info.event.extendedProps.id_paciente)
                $('#modal-edicao-agendamentos').modal('show')
               
            } else {
                enviarMsg("Atendimento já realizado!")
                calendar.refetchEvents();
            }

            $('#close').bind("click", function () {
                calendar.refetchEvents();
            })


        },
        eventDrop: function (info) {

            var data = moment().format('DD-MM-YYYY')
            var dataEvent = moment(info.oldEvent.start).format('DD-MM-YYYY')
            $("#btn-atendimento").css("display", "none");

            if (info.event.extendedProps.status == 1) {
                info.revert();
                enviarMsg("Atendimento já realizado!") 
            } else if (moment(info.oldEvent.start).isAfter(moment()) || data == dataEvent && info.event.extendedProps.status == 0) {

                id = info.event.extendedProps.id_paciente;
                id_ed = info.event.id;
                $('#data_agendamento_start_e').val(moment(info.event.start).format('YYYY-MM-DDTHH:mm'))
                $('#data_agendamento_end_e').val(moment(info.event.end).format('YYYY-MM-DDTHH:mm'))
                $('#paciente_e').val(info.event.title)
                $('#cpf_e').val(info.event.extendedProps.cpf)
                $('#cns_e').val(info.event.extendedProps.cns)
                $('#paciente_id_e').val(info.event.extendedProps.id_paciente)
                $('#modal-edicao-agendamentos').modal('show')

                $('#close').bind("click", function () {
                    calendar.refetchEvents();
                })

            } else {
                info.revert();
                enviarMsg("Não é possivel alterar agendamentos antigos!")
            }

        },
        editable: true,
        eventLimit: true, // allow "more" link when too many events,
        events: {
            url: '/api/agendamentos/listarPorMedico' + localStorage.getItem('user_id'),
            error: function () {

            },
            success: function (dados) {
                return events = dados.agendamentos.map(function (element) {
                    return {
                        title: element.nome, start: element.data_agendamento, end: element.data_agendamento_fim,
                        id: element.id, id_paciente: element.pacientes_id, cpf: element.cpf, cns: element.cns,
                        status: element.status_agendamento, backgroundColor: element.status_agendamento == 0 ? '#4682B4' : '#228B22',
                        borderColor: element.status_agendamento == 0 ? '#4682B4' : '#228B22'
                    }
                })
            }

        }

    });

    calendar.render();

})

function pesquisarPaciente(tipo) {
    let pesquisa = ''

    $('#table-pacientes').DataTable().clear().destroy()

    if ($('#paciente').val() == "" && tipo == 0) {
        enviarMsg("Preencha o campo de pesquisa")
    } else if ($('#paciente_e').val() == "" && tipo == 1) {
        enviarMsg("Preencha o campo de pesquisa")
    } else {

        switch (tipo) {
            case 0:
                pesquisa = $('#paciente').val()
                break;
            case 1:
                pesquisa = $('#paciente_e').val()
                break;
        }

        $('#table-pacientes').DataTable({
            responsive: true,
            autoWidth: false,
            ajax: {
                url: '/api/pacientes/agendamento/' + pesquisa,
                type: 'GET',
                dataSrc: 'pacientes'
            },
            columns: [
                { 'data': 'nome' },
                { 'data': 'cpf' },
                { 'data': 'cns' },
                {
                    'data': 'id', render: (data, type, row) => {
                        return `
                        <div class="btn-group">
                            <button type="button" class="btn btn-secondary btn-sm" data-toggle="modal" data-target="#modal-pacientes" onclick="selecionarPaciente('${row.nome}','${row.cpf}','${row.cns}','${data}','${tipo}')">
                                <i class="fas fa-edit"></i>
                            </button>
                        </div>
                        `;
                    }
                },

            ],
            language: {
                "sEmptyTable": "Nenhum registro encontrado",
                "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
                "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
                "sInfoFiltered": "(Filtrados de _MAX_ registros)",
                "sInfoPostFix": "",
                "sInfoThousands": ".",
                "sLengthMenu": "Exibir _MENU_ resultados por página",
                "sLoadingRecords": "Carregando...",
                "sProcessing": "Processando...",
                "sZeroRecords": "Nenhum registro encontrado",
                "sSearch": "Pesquisar",
                "oPaginate": {
                    "sNext": "Próximo",
                    "sPrevious": "Anterior",
                    "sFirst": "Primeiro",
                    "sLast": "Último"
                },
                "oAria": {
                    "sSortAscending": ": Ordenar colunas de forma ascendente",
                    "sSortDescending": ": Ordenar colunas de forma descendente"
                },
                "select": {
                    "rows": {
                        "_": "Selecionado %d linhas",
                        "0": "Nenhuma linha selecionada",
                        "1": "Selecionado 1 linha"
                    }
                }
            }
        })

        $('#modal-pacientes').modal('show')
    }
}

function selecionarPaciente(nome, cpf, cns, id_pac, tipo) {

    id = id_pac;

    if (tipo == 0) {
        $("#dados").css("display", "flex");
        $('#paciente').val(nome)
        $('#cpf').val(cpf)
        $('#cns').val(cns)
        $('#paciente_id').val(id)
    } else {
        $('#paciente_e').val(nome)
        $('#cpf_e').val(cpf)
        $('#cns_e').val(cns)
        $('#paciente_id_e').val(id)
    }

}

function enviarMsg(msg) {
    toastr.error(msg);
    return false;
}

function validacaoRegistro(tipo) {
    msgErrors = "";

    if (tipo == 0) {
        //Tratar campos vazios
        if ($('#paciente').val() == "" ||
            $('#cns').val() == "" ||
            $('#paciente_id').val() == "" ||
            $('#data_agendamento_start').val() == "" ||
            $('#data_agendamento_end').val() == "" ||
            id == "") {
            msgErrors = "Verifique se todos campos obrigatórios estão preenchidos!"
        }

    } else {
        //Tratar campos vazios
        if ($('#paciente_e').val() == "" ||
            $('#cns_e').val() == "" ||
            $('#paciente_id_e').val() == "" ||
            $('#data_agendamento_start_e').val() == "" ||
            $('#data_agendamento_end_e').val() == "" ||
            id == "") {
            msgErrors = "Verifique se todos campos obrigatórios estão preenchidos!"
        }

    }


    //Verificando se tem erro ou não
    if (msgErrors) {
        enviarMsg(msgErrors)
    } else {
        enviarDados(tipo)
    }
}

function enviarDados(tipo) {
    var dados = {};

    if (tipo == 0) {
        $('.form-agendamentos').serializeArray().map(function (x) { dados[x.name] = x.value; });

        dados.paciente_id = id;
        dados.medico_id = localStorage.getItem('user_id');
        delete dados.paciente
        delete dados.cpf
        delete dados.cns

        $.ajax({
            url: '/api/agendamentos/',
            type: 'post',
            dataType: 'json',
            async: true,
            data: dados,
        }).done(function (callback) {
            toastr.success(callback.message)
            $('#modal-agendamentos').modal('hide');
            calendar.refetchEvents();
        }).fail(function (callback) {
            msgErro = JSON.parse(callback.responseText)
            enviarMsg(msgErro.message)
        })

    } else {
        $('.form-edicao-agendamentos').serializeArray().map(function (x) { dados[x.name] = x.value; });

        dados.medico_id = localStorage.getItem('user_id');
        delete dados.paciente_e
        delete dados.cpf_e
        delete dados.cns_e

        $.ajax({
            url: '/api/agendamentos/' + id_ed,
            type: 'put',
            dataType: 'json',
            async: true,
            data: dados,
        }).done(function (callback) {
            toastr.success(callback.message)
            $('#modal-edicao-agendamentos').modal('hide');
            calendar.refetchEvents();
        }).fail(function (callback) {
            msgErro = JSON.parse(callback.responseText)
            enviarMsg(msgErro.message)
        })
    }
}

function desmarcarAgendamento() {
    bootbox.confirm({
        message: "Deseja desmarcar o agendamento ?",
        buttons: {
            confirm: {
                label: 'Sim',
                className: 'btn-danger',
                type: 'button'
            },
            cancel: {
                label: 'Não',
                className: 'btn-default'
            },
        },
        callback: function (result) {
            if (result == true) {
                $.ajax({
                    url: '/api/agendamentos/' + id_ed,
                    type: 'delete',
                    async: true,
                }).done(function (callback) {
                    toastr.success(callback.message)
                    $('#modal-edicao-agendamentos').modal('hide');
                    calendar.refetchEvents();
                }).fail(function (callback) {
                    msgErro = JSON.parse(callback.responseText)
                    enviarMsg(msgErro.message)
                })
            }
        }
    })
}

function realizarAtendimento() {
    bootbox.confirm({
        message: "Deseja atender o paciente ?",
        buttons: {
            confirm: {
                label: 'Sim',
                className: 'btn btn-primary',
                type: 'button'
            },
            cancel: {
                label: 'Não',
                className: 'btn btn-danger'
            },
        },
        callback: function (result) {
            if (result == true) {
                window.location.replace("/medicos/atendimento?" + id_ed);
            }
        }
    })
}