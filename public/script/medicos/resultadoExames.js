function pesquisarExame(){
    if($('#exame').val() == ""){
        toastr.error("Preencha o campo de pesquisa")
    }else {
        $.ajax({
            url: '/api/exames/resultadoExames/' + $('#exame').val(),
            type: "get",
            dataType: "json",
            assync: true
        }).done(function (callback){
            if(callback.exames.length){
                criarTabela(callback.exames)
            } else {
                toastr.error("Não existem exames com esse ID")
            }
        })
    }
}
function criarTabela(exames){

    $('#nome_pac').val(exames[0].nome_paciente)
    $('#exames').css("display", "block");
    $("#tbodyDownloads tr").remove()

    if (exames.length) {
        for (i = 0; i < exames.length; i++) {
            
            var path = "../" +  exames[i].path;

            if(exames[i].status_entrega == '0'){
                newTrItem = $("<tr>" +
                "<td>" + exames[i].nome_exame + "</td>" +
                "<td>" + exames[i].data_solicitacao + "</td>" +
                "<td> Ainda não enviado</td>" +
                "<td> Ainda não enviado</td>" +
                "</tr>")
            } else {
                newTrItem = $("<tr>" +
                "<td>" + exames[i].nome_exame + "</td>" +
                "<td>" + exames[i].data_solicitacao + "</td>" +
                "<td>" + exames[i].data_entrega + "</td>" +
                "<td><a href='" + path + "' download='" + exames[i].nome_original + "'>Baixar Arquivo</a></td>" +
                "</tr>")
            }
            
            appendTable(newTrItem)
        }
    } else {
        newTrItem = $("<tr class='danger'><td colspan='6'>Nenhum resultado encontrado.</td></tr>")
        appendTable(newTrItem)
    }
}

function appendTable(newTrItem) {
    $("#tbodyDownloads").append(newTrItem)
}
