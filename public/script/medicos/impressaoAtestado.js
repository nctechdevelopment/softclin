function imprimirAtestado() {

    if ($('#periodo').val() == '' ||
        $('#cid').val() == '') {
        toastr.error("Preencha todos os campos!")
    } else {
        var pdf = new jsPDF();

        pdf.text('Atestado Médico', 90, 20)
        pdf.setLineWidth(0.5)
        pdf.line(20, 25, 190, 25)

        pdf.text('Paciente: ' + $('#nome_pac').val(), 20, 40)

        pdf.text('Atesto para os devidos fins, a pedido do interessado, que o paciente ', 20 ,60)
        pdf.text('foi submetido à consulta médica no seguinte periodo: ', 20 ,70)
        pdf.text(moment().format('DD/MM/YYYY HH:mm') + ' as ' + moment().add(1, 'hour').format('DD/MM/YYYY HH:mm'), 20, 80)

        
        pdf.text('Em decorrência, deverá permanecer afastado de suas atividades', 20 ,100)
        pdf.text('laborativas por um período de: ' + $('#periodo').val() + '.', 20 ,110)

        pdf.text('CID da doença: ' + $('#cid').val(), 20, 120)
        
        pdf.text('Dr: ' + $('#nome_medi').val(), 20, 260)

        pdf.setLineWidth(0.5)
        pdf.line(20, 270, 190, 270)
        pdf.text(moment().format('DD/MM/YYYY HH:mm'), 20, 280)
        pdf.text('Válido a partir desta data!', 90, 280)

        pdf.save('Atestado Médico')
    }

}
