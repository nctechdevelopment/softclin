'use strict'

function imprimirExame(id_consulta, senha){
    $.ajax({
        url: '/api/exames/examesConsulta/' + id_consulta,
        type: 'get',
        dataType: 'json',
        async: true,
    }).done(function (callback) {
        if(callback.exames.length){
            criarExame(callback.exames, id_consulta)
        } else {
            toastr.error("Não foram solicitados exames nessa consulta!")
        }
        
    })
}

function criarExame(exames, id_consulta) {

    var pdf = new jsPDF();
    var diaAtual = moment().format('DD/MM/YYYY')

    pdf.text('Pedido de Exame', 90, 20)
    pdf.setLineWidth(0.5)
    pdf.line(20, 25, 190, 25) 

    pdf.text('Paciente: ' + exames[0].nome_paciente, 20, 40)

    var linha = 60;

    for(let i = 0; i < exames.length; i++){

        pdf.text(exames[i].nome_exame, 20 , linha)
        linha = linha+10;
        
        pdf.text('Descrição: ' + exames[i].descricao, 20, linha)
        linha = linha+20;
    }

    

    pdf.text('Dr: ' + exames[0].nome_medico + " / CRM: " + exames[0].crm , 60, 200)

    pdf.text(exames[0].rua + " , N: " + exames[0].numero, 60, 210)
    pdf.text(exames[0].bairro + ", " + exames[0].cidade + " - " + exames[0].uf, 60, 220)
    pdf.text(exames[0].celular_medico + " / " + exames[0].telefone, 60, 230)

    pdf.text("ID do exame para envio do Laboratório: " + id_consulta, 20, 250)
    pdf.text("Senha para visualizar os exames: " + exames[0].senha, 20, 260)
    pdf.setLineWidth(0.5)
    pdf.line(20, 270, 190, 270)
    pdf.text(diaAtual, 20, 280)
    pdf.text('Pedido eletronico gerado por Softclin!', 90, 280)
    
    pdf.save('Exame')
}