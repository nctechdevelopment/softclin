//Formulário de login
jQuery(document).ready(function(){
    $('.form-users').submit(function (event){
        event.preventDefault()
        validacaoLogin()
    })
})

//Validação
function validacaoLogin() {
    //Mensagens de erros
    msgErros = ""

    //Campos Vazios
    if ($('#idExame').val() == "" || $('#txtSenha').val() == ""){
        msgErros = ("Preencha todos os campos.")
    }

    //Envia msg erros
    if (msgErros) {
        enviarMsg(msgErros)
    } else {
        enviarMsg(msgErros)
        $('#divResult').removeClass("alert-danger")
        enviarDados()
    }

}

//Mostra mensagens de erros
function enviarMsg(msg){
    $('#divResult').addClass("alert-danger")
    $('#result').html(msg)
    $('#divResult').fadeIn(500)
    return false
}

// Envia forms via AJAX
function enviarDados(){
    $.ajax({
        url: "/api/access/loginExame",
        type: "post",
        dataType: "json",
        async: true,
        data: $("form").serialize()
    }).done(function(data){   
        localStorage.setItem('user_id', data.user.id)
        localStorage.setItem('nome', data.user.nome)
        toastr.options.onHidden = function() { window.location = "/acesso-exames?" + data.user.consultaID }
        toastr.options.timeOut = 1;
        toastr.success(data.info)
    }).fail(function (callback){
        callbackMsg = JSON.parse(callback.responseText)
        enviarMsg(callbackMsg.info)
    })
}