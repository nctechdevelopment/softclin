'use strict'

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) 
        return next()
    else
        res.redirect('/api/access/logout')
}

function isAuthorized(access) {
    return function (req, res, next) {
        
        if (access.indexOf(req.session.passport.user.nivel_acesso) != -1)
            return next()
        else
            res.redirect('/api/access/logout')
    }
}

module.exports = {
    isLoggedIn,
    isAuthorized
};