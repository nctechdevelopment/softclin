'use strict'

const connection = require('../database/connection'),
    httpStatus = require('http-status')

var ReceitasService = {

    cadastrar: async function (dados, callback, error) {

        try {
            let sql = 'INSERT INTO prescricao (consultas_id, medicamentos_id, posologia) VALUES (?, ?, ?)'

            const [result] = await connection.query(sql, [dados.consulta_id, dados.medicamento_id, dados.posologia])

            callback(null, httpStatus.OK, 'Medicamento prescrito com sucesso.')

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    },

    medicamentosReceita: async function (id, callback, error) {

        try {
            let sql = 'SELECT medicos.nome as nome_medico, pacientes.nome as nome_paciente, medicamentos.nome_fabrica, medicos.crm, '+
                    'medicamentos.nome_generico, medicamentos.composicao, medicamentos.descricao, prescricao.posologia, '+
                    'medicos.rua, medicos.numero, medicos.cidade, medicos.uf, medicos.bairro, medicos.celular, medicos.telefone FROM prescricao '+
                    'JOIN consultas ON consultas_id = consultas.id '+
                    'JOIN medicos ON consultas.medicos_id = medicos.id '+
                    'JOIN pacientes ON consultas.pacientes_id = pacientes.id ' +
                    'JOIN medicamentos ON medicamentos_id = medicamentos.id ' +
                    'WHERE consultas_id=?'

            const [result] = await connection.query(sql,id)

            callback(null, result)

        } catch (error) {
            console.log(error)
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    },


}

module.exports = ReceitasService