'use strict'

const connection = require('../database/connection'),
    httpStatus = require('http-status'),
    duplicidade = require('../utils/checarDuplicidade'),
    HomeController = require('../controller/HomeController')

var LabService = {

    cadastrar: async function (dados, callback, error) {

        const tabelas = ['laboratorios', 'laboratorios', 'laboratorios', 'laboratorios', 'login']
        const campos = ['razao_social', 'cnpj', 'celular', 'telefone', 'email']
        const chaves = ['razão social', 'cnpj', 'celular', 'telefone', 'email']

        let checarMsg = await duplicidade.checarDuplicidade(dados, campos, tabelas, chaves)

        if (checarMsg) {
            callback(error, httpStatus.CONFLICT, 'Campo(s) duplicado(s): ' + checarMsg.replace(/.$/, "") + '.')
        } else {

            let nivel_acesso = 2;
            let resultID = '';
            let status = 1;

            await HomeController.cadastrar(dados, nivel_acesso, async function(error, loginID){
                resultID = loginID;
            })

            try {
                let sql = 'INSERT INTO laboratorios (login_id, nome_fantasia, cnpj, razao_social, celular, telefone,' +
                    'cep, rua, bairro, cidade, complemento, uf, numero, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'
                    
                const [result] = await connection.query(sql, [resultID, dados.nome_fantasia, dados.cnpj, dados.razao_social, 
                    dados.celular, dados.telefone, dados.cep, dados.rua, dados.bairro, dados.cidade, dados.complemento, 
                    dados.uf, dados.numero, status])

                callback(null, httpStatus.OK, 'Cadastro efetudado com sucesso.')

            } catch (error) {
                callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
            }
        }

    },

    listar: async function (callback) {

        try {
            let sql = 'SELECT id, nome_fantasia, cnpj, status FROM laboratorios ORDER BY id'

            const [result] = await connection.query(sql)

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    listarPorId: async function (id, callback) {

        try {
            let sql = 'SELECT id, nome_fantasia, cnpj, razao_social,' +
                ' celular, telefone, cep, rua, bairro, cidade, complemento, uf, numero' +
                ' FROM laboratorios WHERE id=? LIMIT 1'

            const [result] = await connection.query(sql, id)

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    editar: async function (id, dados, callback, error) {

        const tabelas = ['laboratorios', 'laboratorios', 'laboratorios', 'laboratorios']
        const campos = ['razao_social', 'cnpj', 'celular', 'telefone']
        const chaves = ['razão social', 'cnpj', 'celular', 'telefone']

        let checarMsg = await duplicidade.checarDuplicidade(dados, campos, tabelas, chaves, id)

        if (checarMsg) {
            callback(error, httpStatus.CONFLICT, 'Campo(s) duplicado(s): ' + checarMsg.replace(/.$/, "") + '.')
        } else {

            try {

                    let sql = 'UPDATE laboratorios SET nome_fantasia=?, cnpj=?, razao_social=?, celular=?, telefone=?,' +
                        'cep=?, rua=?, bairro=?, cidade=?, complemento=?, uf=?, numero=? WHERE id=?'

                    const [result] = await connection.query(sql, [dados.nome_fantasia, dados.cnpj, dados.razao_social,
                        dados.celular, dados.telefone, dados.cep, dados.rua, dados.bairro, dados.cidade, dados.complemento,
                        dados.uf, dados.numero, id])

                    callback(null, httpStatus.OK, 'Edição efutada com sucesso.')


            } catch (error) {
                callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
            }
        }
    },


    mudarStatus: async function (id, data, callback) {
        try {


            let sql = 'UPDATE laboratorios SET status=? WHERE id=?'

            const [result] = await connection.query(sql, [data.status, id])


            if (data.status == 1) {
                callback(null, httpStatus.OK, 'Usuário habilitado com sucesso!')
            } else {
                callback(null, httpStatus.OK, 'Usuário desabilitado com sucesso!')
            }

        } catch (error) {
            console.log(error)
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    }
}

module.exports = LabService