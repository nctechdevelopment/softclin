'use strict'

const connection = require('../database/connection'),
    httpStatus = require('http-status')

var ExamesService = {

    cadastrar: async function (dados, callback, error) {

        try {
            let sql = 'INSERT INTO exames (consultas_id, tipos_exames_id, descricao, senha) VALUES (?, ?, ?, ?)'

            const [result] = await connection.query(sql, [dados.consulta_id, dados.exame_id, dados.descricao_ex, dados.senha])

            callback(null, httpStatus.OK, 'Exame solicitado com sucesso.')

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    },

    examesConsulta: async function (id, callback, error) {

        try {
            let sql = 'SELECT medicos.nome as nome_medico, pacientes.nome as nome_paciente, medicos.crm, exames.id as id_solicitacao, ' +
                'tipos_exames.nome as nome_exame, exames.descricao, medicos.rua, medicos.numero, medicos.cidade, exames.senha, ' +
                'medicos.uf, pacientes.cns, pacientes.cpf, pacientes.celular as celular_paciente, tipos_exames_id, ' +
                'data_solicitacao, medicos.bairro, medicos.celular as celular_medico, medicos.telefone, status_entrega FROM exames ' +
                'JOIN consultas ON consultas_id = consultas.id ' +
                'JOIN medicos ON consultas.medicos_id = medicos.id ' +
                'JOIN pacientes ON consultas.pacientes_id = pacientes.id ' +
                'JOIN tipos_exames ON tipos_exames_id = tipos_exames.id ' +
                'WHERE consultas_id=?'

            const [result] = await connection.query(sql, id)

            callback(null, result)

        } catch (error) {
            console.log(error)
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    },

    downloadExames: async function (id_cons, id_pac, callback, error) {

        try {
            let sql = 'SELECT tipos_exames.nome as nome_exame, path, nome_original, '+
                'DATE_FORMAT(data_entrega, "%d-%m-%Y") AS data_entrega FROM exames ' +
                'JOIN consultas ON consultas_id = consultas.id ' +
                'JOIN pacientes ON consultas.pacientes_id = pacientes.id ' +
                'JOIN tipos_exames ON tipos_exames_id = tipos_exames.id ' +
                'WHERE consultas_id=? AND pacientes.id=? AND status_entrega="1"'

            const [result] = await connection.query(sql, [id_cons, id_pac])

            callback(null, result)

        } catch (error) {
            console.log(error)
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    },

    resultadoExames: async function (id_cons, callback, error) {

        try {
            let sql = 'SELECT tipos_exames.nome as nome_exame, path, ' +
                'pacientes.nome as nome_paciente, nome_original, status_entrega, '+
                'DATE_FORMAT(data_entrega, "%d-%m-%Y") AS data_entrega, ' +
                'DATE_FORMAT(data_solicitacao, "%d-%m-%Y") AS data_solicitacao  FROM exames ' +
                'JOIN consultas ON consultas_id = consultas.id ' +
                'JOIN pacientes ON consultas.pacientes_id = pacientes.id ' +
                'JOIN tipos_exames ON tipos_exames_id = tipos_exames.id ' +
                'WHERE consultas_id=?'

            const [result] = await connection.query(sql, id_cons)

            callback(null, result)

        } catch (error) {
            console.log(error)
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    },

    pesquisar: async function (pesquisa, callback) {

        try {
            let sql = `SELECT * FROM tipos_exames WHERE nome LIKE "%${pesquisa}%"`

            const [result] = await connection.query(sql, pesquisa)

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    enviarExames: async function (id, path, dados, callback, error) {
        try {
            let [result] = '',
                status = '1'

            let sql = 'UPDATE exames SET path=?, nome_original=?, data_entrega=?, status_entrega=? WHERE consultas_id=? AND id=?'

            if (Array.isArray(dados.exame)) {
                for (let i = 0; i < dados.exame.length; i++) {

                    [result] = await connection.query(sql, [path, dados.nomeOriginal,
                        dados.dataAtual, status, id, dados.exame[i]])
                }
            } else {
                [result] = await connection.query(sql, [path, dados.nomeOriginal,
                    dados.dataAtual, status, id, dados.exame])
            }


            callback(null, httpStatus.OK, 'Exames enviados com sucesso.')
        } catch (error) {
            console.log(error)
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }


    }


}

module.exports = ExamesService