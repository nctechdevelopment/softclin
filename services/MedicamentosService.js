'use strict'

const connection = require('../database/connection'),
    httpStatus = require('http-status')

var MedicamentosService = {

    cadastrar: async function (dados, callback, error) {

        try {
            let sql = 'INSERT INTO medicamentos (nome_generico, nome_fabrica, composicao, descricao, fabricante )' +
                'VALUES (?, ?, ?, ?, ?)'

            const [result] = await connection.query(sql, [dados.nome_generico, dados.nome_fabrica,
            dados.composicao, dados.descricao, dados.fabricante])

            callback(null, httpStatus.OK, 'Cadastro efetudado com sucesso.')

        } catch (error) {
            console.log(error)
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    },

    listar: async function (callback) {

        try {
            let sql = 'SELECT id, nome_generico, nome_fabrica, descricao FROM medicamentos ORDER BY id'

            const [result] = await connection.query(sql)

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    pesquisar: async function (pesquisa, callback) {

        try {
            let sql = `SELECT * FROM medicamentos WHERE nome_generico LIKE "%${pesquisa}%" OR nome_fabrica  LIKE "%${pesquisa}%" OR composicao LIKE "%${pesquisa}%"`

            const [result] = await connection.query(sql, [pesquisa, pesquisa, pesquisa])

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    listarPorId: async function (id, callback) {

        try {
            let sql = 'SELECT * FROM medicamentos WHERE id=? LIMIT 1'

            const [result] = await connection.query(sql, id)

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    editar: async function (id, dados, callback, error) {

        try {

            let sql = 'UPDATE medicamentos SET nome_generico=?, nome_fabrica=?, composicao=?, descricao=?, fabricante=? WHERE id=?'

            const [result] = await connection.query(sql, [dados.nome_generico, dados.nome_fabrica, dados.composicao, dados.descricao, dados.fabricante, id])

            callback(null, httpStatus.OK, 'Edição efutada com sucesso.')


        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    }
}

module.exports = MedicamentosService