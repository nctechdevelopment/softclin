'use strict'

const connection = require('../database/connection'),
    httpStatus = require('http-status'),
    duplicidade = require('../utils/checarDuplicidade')

var PacienteService = {

    cadastrar: async function (dados, callback, error) {

        const tabelas = ['pacientes', 'pacientes', 'pacientes']
        const campos = ['cpf', 'rg', 'cns']
        const chaves = ['cpf', 'rg', 'cns']

        let status = 1;

        let checarMsg = await duplicidade.checarDuplicidade(dados, campos, tabelas, chaves)

        if (checarMsg) {
            callback(error, httpStatus.CONFLICT, 'Campo(s) duplicado(s): ' + checarMsg.replace(/.$/, "") + '.')
        } else {

            try {
                let sql = 'INSERT INTO pacientes (nome, data_nascimento, cpf, rg, cns, nome_resp, cpf_resp, tipo_sanguineo,' +
                    'sexo, celular, telefone, cep, rua, bairro, cidade, complemento, uf, numero, doencas, limitacao_fis, alergia, status)' +
                    'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'

                const [result] = await connection.query(sql, [dados.nome, dados.data_nascimento, dados.cpf, dados.rg, dados.cns,
                dados.nome_resp, dados.cpf_resp, dados.tipo_sanguineo, dados.sexo, dados.celular, dados.telefone, dados.cep,
                dados.rua, dados.bairro, dados.cidade, dados.complemento, dados.uf, dados.numero, dados.doencas, dados.limitacao_fis, dados.alergia, status])

                callback(null, httpStatus.OK, 'Cadastro efetudado com sucesso.')

            } catch (error) {
                console.log(error)
                callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
            }
        }
    },

    listar: async function (callback) {

        try {
            let sql = 'SELECT id, nome, cns, status FROM pacientes ORDER BY id'

            const [result] = await connection.query(sql)

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    listarPorId: async function (id, callback) {

        try {
            let sql = 'SELECT DATE_FORMAT(data_nascimento, "%Y-%m-%d") AS data_nascimento, nome, id, cpf, rg,' +
                ' cns, nome_resp, cpf_resp, tipo_sanguineo, sexo, celular, telefone, cep, rua, bairro,' +
                ' cidade, complemento, uf, numero, doencas, limitacao_fis, alergia FROM pacientes WHERE id=? LIMIT 1'

            const [result] = await connection.query(sql, id)

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    editar: async function (id, dados, callback, error) {

        const tabelas = ['pacientes', 'pacientes', 'pacientes']
        const campos = ['cpf', 'rg', 'cns']
        const chaves = ['cpf', 'rg', 'cns']

        let checarMsg = await duplicidade.checarDuplicidade(dados, campos, tabelas, chaves, id)

        if (checarMsg) {
            callback(error, httpStatus.CONFLICT, 'Campo(s) duplicado(s): ' + checarMsg.replace(/.$/, "") + '.')
        } else {

            try {

                let sql = 'UPDATE pacientes SET nome=?, data_nascimento=?, cpf=?, rg=?, cns=?, nome_resp=?, cpf_resp=?,' +
                    ' tipo_sanguineo=?, sexo=?, celular=?, telefone=?, cep=?, rua=?, bairro=?, cidade=?, complemento=?,' +
                    ' uf=?, numero=?, doencas=?, limitacao_fis=?, alergia=? WHERE id=?'

                const [result] = await connection.query(sql, [dados.nome, dados.data_nascimento, dados.cpf, dados.rg,
                dados.cns, dados.nome_resp, dados.cpf_resp, dados.tipo_sanguineo, dados.sexo, dados.celular,
                dados.telefone, dados.cep, dados.rua, dados.bairro, dados.cidade, dados.complemento,
                dados.uf, dados.numero, dados.doencas, dados.limitacao_fis, dados.alergia, id])

                callback(null, httpStatus.OK, 'Edição efutada com sucesso.')


            } catch (error) {
                callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
            }
        }
    },

    pesquisar: async function (pesquisa, callback) {

        try {
            let sql = `SELECT id, nome, cpf, cns FROM pacientes WHERE (nome LIKE "%${pesquisa}%" OR cpf  LIKE "%${pesquisa}%" OR cns LIKE "%${pesquisa}%") AND (status = 1)`

            const [result] = await connection.query(sql, [pesquisa, pesquisa, pesquisa])

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    mudarStatus: async function (id, data, callback) {
        try {


            let sql = 'UPDATE pacientes SET status=? WHERE id=?'

            const [result] = await connection.query(sql, [data.status, id])


            if (data.status == 1) {
                callback(null, httpStatus.OK, 'Usuário habilitado com sucesso!')
            } else {
                callback(null, httpStatus.OK, 'Usuário desabilitado com sucesso!')
            }

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    }
}

module.exports = PacienteService