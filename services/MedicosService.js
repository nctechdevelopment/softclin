'use strict'

const connection = require('../database/connection'),
    httpStatus = require('http-status'),
    duplicidade = require('../utils/checarDuplicidade'),
    HomeController = require('../controller/HomeController')

var MedService = {

    cadastrar: async function (dados, callback, error) {

        const tabelas = ['medicos', 'medicos', 'medicos', 'medicos', 'login']
        const campos = ['cpf', 'rg', 'crm', 'celular', 'email']
        const chaves = ['cpf', 'rg', 'crm', 'celular', 'email']

        let checarMsg = await duplicidade.checarDuplicidade(dados, campos, tabelas, chaves)

        if (checarMsg) {
            callback(error, httpStatus.CONFLICT, 'Campo(s) duplicado(s): ' + checarMsg.replace(/.$/, "") + '.')
        } else {

            let nivel_acesso = 3;
            let status = 1;
            let resultID = '';

            await HomeController.cadastrar(dados, nivel_acesso, async function (error, loginID) {
                resultID = loginID;
            })

            try {
                let sql = 'INSERT INTO medicos (login_id, nome, data_nascimento, cpf, rg, crm, celular, telefone,' +
                    'cep, rua, bairro, cidade, complemento, uf, numero, status) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'

                const [result] = await connection.query(sql, [resultID, dados.nome, dados.data_nascimento, dados.cpf, dados.rg,
                    dados.crm, dados.celular, dados.telefone, dados.cep, dados.rua, dados.bairro, dados.cidade, dados.complemento,
                    dados.uf, dados.numero, status])

                callback(null, httpStatus.OK, 'Cadastro efetudado com sucesso.')

            } catch (error) {
                callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
            }
        }

    },

    listar: async function (callback) {

        try {
            let sql = 'SELECT id, nome, cpf, status FROM medicos ORDER BY id'

            const [result] = await connection.query(sql)

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    listarPorId: async function (id, callback) {

        try {
            let sql = 'SELECT DATE_FORMAT(data_nascimento, "%Y-%m-%d") AS data_nascimento, nome, id, cpf, rg,' +
                ' crm, celular, telefone, cep, rua, bairro, cidade, complemento, uf, numero ' +
                ' FROM medicos WHERE id=? LIMIT 1'

            const [result] = await connection.query(sql, id)

           
            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    medicosDisp: async function (callback) {

        try {
            let sql = 'SELECT id, nome FROM medicos WHERE status = "1" ORDER BY id'

            const [result] = await connection.query(sql)

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    editar: async function (id, dados, callback, error) {

        const tabelas = ['medicos', 'medicos', 'medicos', 'medicos']
        const campos = ['cpf', 'rg', 'crm', 'celular']
        const chaves = ['cpf', 'rg', 'crm', 'celular']

        let checarMsg = await duplicidade.checarDuplicidade(dados, campos, tabelas, chaves, id)

        if (checarMsg) {
            callback(error, httpStatus.CONFLICT, 'Campo(s) duplicado(s): ' + checarMsg.replace(/.$/, "") + '.')
        } else {

            try {

                    let sql = 'UPDATE medicos SET nome=?, data_nascimento=?, cpf=?, rg=?, crm=?, celular=?, telefone=?,' +
                        'cep=?, rua=?, bairro=?, cidade=?, complemento=?, uf=?, numero=? WHERE id=?'

                    const [result] = await connection.query(sql, [dados.nome, dados.data_nascimento, dados.cpf, dados.rg,
                        dados.crm, dados.celular, dados.telefone, dados.cep, dados.rua, dados.bairro, dados.cidade, dados.complemento,
                        dados.uf, dados.numero, id])

                    callback(null, httpStatus.OK, 'Edição efutada com sucesso.')


            } catch (error) {
                callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
            }
        }
    },

    mudarStatus: async function (id, data, callback) {
        try {


            let sql = 'UPDATE medicos SET status=? WHERE id=?'

            const [result] = await connection.query(sql, [data.status, id])


            if (data.status == 1) {
                callback(null, httpStatus.OK, 'Usuário habilitado com sucesso!')
            } else {
                callback(null, httpStatus.OK, 'Usuário desabilitado com sucesso!')
            }

        } catch (error) {
            console.log(error)
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    }
}

module.exports = MedService