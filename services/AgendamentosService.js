'use strict'

const connection = require('../database/connection'),
    httpStatus = require('http-status')

var AgendamentosService = {

    cadastrar: async function (dados, callback, error) {

        let status = '0';

        try {
            let sql = 'INSERT INTO agendamentos (pacientes_id, medicos_id, status_agendamento, data_agendamento, ' +
                'data_agendamento_fim ) VALUES (?, ?, ?, ?, ?)'

            const [result] = await connection.query(sql, [dados.paciente_id, dados.medico_id, status,
            dados.data_agendamento_start, dados.data_agendamento_end])

            callback(null, httpStatus.OK, 'Agendamento efetudado com sucesso.')

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    },

    editar: async function (id, dados, callback, error) {

        try {

            let sql = 'UPDATE agendamentos SET pacientes_id=?, medicos_id=?, data_agendamento=?, ' +
                'data_agendamento_fim=? WHERE id=?'

            const [result] = await connection.query(sql, [dados.paciente_id_e, dados.medico_id,
            dados.data_agendamento_start_e, dados.data_agendamento_end_e, id])

            callback(null, httpStatus.OK, 'Alteração efutada com sucesso.')


        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    },

    excluir: async function (id, callback, error) {

        try {

            let sql = 'DELETE FROM agendamentos WHERE id=?'

            const [result] = await connection.query(sql, id)

            callback(null, httpStatus.OK, 'Agendamento desmarcado com sucesso!')


        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    },

    listarPorMedico: async function (id, callback) {

        try {
            let sql = 'SELECT agendamentos.id, data_agendamento, data_agendamento_fim, pacientes_id, ' +
                'status_agendamento, nome, cpf, cns ' +
                'FROM agendamentos JOIN pacientes ON pacientes_id = pacientes.id WHERE medicos_id=?;'

            const [result] = await connection.query(sql, id)

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    retornarAgendamento: async function (id_agend, id_med, callback) {

        try {
            let sql = 'SELECT medicos.nome as nome_medico, pacientes.nome as nome_paciente, pacientes.id as id_paciente, pacientes.data_nascimento, tipo_sanguineo, ' +
                'cns, pacientes.sexo, pacientes.celular, pacientes.telefone, ' +
                'doencas, limitacao_fis, alergia ' +
                'FROM agendamentos JOIN pacientes ON pacientes_id = pacientes.id ' +
                'JOIN medicos ON medicos_id = medicos.id WHERE agendamentos.id=? AND medicos_id=? ' +
                'AND DATE_FORMAT(data_agendamento, "%d-%m-%Y") = DATE_FORMAT(curdate(), "%d-%m-%Y") AND status_agendamento="0";'

            const [result] = await connection.query(sql, [id_agend, id_med])

            callback(null, result)

        } catch (error) {
            console.log(error)
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    mudarStatus: async function (id, status, callback) {

        console.log(status)
        
        try {

            let sql = 'UPDATE agendamentos SET status_agendamento=? WHERE id=?'

            const [result] = await connection.query(sql, [status, id])

        } catch (error) {
            console.log(error)
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    }

}

module.exports = AgendamentosService