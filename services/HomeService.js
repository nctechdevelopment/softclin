'use strict'

const bcrypt = require('bcryptjs'),
    connection = require('../database/connection'),
    httpStatus = require('http-status'),
    duplicidade = require('../utils/checarDuplicidade')

var HomeService = {

    access: async function (txtEmail, txtSenha, callback) {

        try {

            let sql = 'SELECT * FROM login WHERE email = ? LIMIT 1'
            const [result] = await connection.query(sql, txtEmail)

            if (result == 0) {
                callback(new Error(), httpStatus.UNAUTHORIZED, 'Usuário ou senha inválida!')
            } else {
                var tabela = '',
                    campo = ''

                switch (result[0].nivel_acesso) {
                    case "1":
                        tabela = 'administradores';
                        campo = 'nome';
                        break;
                    case "2":
                        tabela = 'laboratorios';
                        campo = 'nome_fantasia';
                        break;
                    case "3":
                        tabela = 'medicos';
                        campo = 'nome';
                        break;
                    case "4":
                        tabela = 'recepcionistas';
                        campo = 'nome';
                        break;
                }

                let sqlStatus = `SELECT id, status, ${campo} FROM ${tabela} WHERE login_id = ? LIMIT 1`
                const [resultStatus] = await connection.query(sqlStatus, result[0].id)

                if (resultStatus[0].status == 0) {
                    callback(new Error(), httpStatus.UNAUTHORIZED, 'Usuário desabilitado!')
                } else if (bcrypt.compareSync(txtSenha, result[0].senha)) {
                    delete result[0].senha

                    
                    const resultConcat = {
                        id: resultStatus[0].id,
                        id_login: result[0].id,
                        nivel_acesso: result[0].nivel_acesso,
                        nome: resultStatus[0].nome ? resultStatus[0].nome : resultStatus[0].nome_fantasia
                    }
                    
                    callback(null, httpStatus.OK, 'Login efetudado com sucesso.', resultConcat)
                } else {
                    callback(new Error(), httpStatus.UNAUTHORIZED, 'Usuário ou senha inválida!')
                }
            }

        } catch (err) {
            console.log(err)
            callback(err, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    },

    accessExam: async function (idExame, txtSenha, callback) {

        try {

            let sql = 'SELECT pacientes.nome, pacientes.id, status_entrega, senha FROM exames ' +
            'JOIN consultas ON consultas_id = consultas.id ' +
            'JOIN pacientes ON consultas.pacientes_id = pacientes.id WHERE consultas_id = ?'
            const [result] = await connection.query(sql, idExame)

            if (result == 0) {
                callback(new Error(), httpStatus.UNAUTHORIZED, 'ID ou senha inválidos!')
            } else if(txtSenha === result[0].senha){
                const exam = result.filter((exames) => {
                    return exames.status_entrega === '1'
                })

                if(exam == 0){
                    callback(new Error(), httpStatus.UNAUTHORIZED, 'Nenhum exame foi enviado ainda!')
                }else {
                    delete result[0].senha

                    const resultado = {
                        id: result[0].id,
                        nivel_acesso: '5',
                        nome: result[0].nome,
                        consultaID: idExame
                    }

                    callback(null, httpStatus.OK, 'Login efetudado com sucesso.', resultado)
                    
                }
            } else {
                callback(new Error(), httpStatus.UNAUTHORIZED, 'ID ou senha inválidos!')
            }

        } catch (err) {
            console.log(err)
            callback(err, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    },

    cadastrar: async function (dados, nivel_acesso, callback) {

        try {
            let sql = 'INSERT INTO login (email, senha,nivel_acesso) VALUES (?, ?, ?)',
                senhaCrip = bcrypt.hashSync(dados.senha, 10)

            const [result] = await connection.query(sql, [dados.email, senhaCrip, nivel_acesso])

            callback(null, httpStatus.OK, "Cadastro de login efeturado com sucesso", result.insertId)

        } catch (err) {
            callback(err, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    },

    listar: async function (callback) {

        try {
            let sql = 'SELECT id, email, senha FROM login ORDER BY id'

            const [result] = await connection.query(sql)

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    listarPorId: async function (id, callback) {

        try {
            let sql = 'SELECT id, email FROM login WHERE id=? LIMIT 1'

            const [result] = await connection.query(sql, id)

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    editar: async function (id, dados, callback, error) {

        const tabelas = ['login']
        const campos = ['email']
        const chaves = ['e-mail']

        let checarMsg = await duplicidade.checarDuplicidade(dados, campos, tabelas, chaves, id)

        if (checarMsg) {
            callback(error, httpStatus.CONFLICT, 'Campo(s) duplicado(s): ' + checarMsg.replace(/.$/, "") + '.')
        } else {

            try {

                let sql = 'UPDATE login SET email=?, senha=? WHERE id=?',
                    senhaCrip = bcrypt.hashSync(dados.senha, 10)

                const [result] = await connection.query(sql, [dados.email, senhaCrip, id])

                callback(null, httpStatus.OK, 'Edição efutada com sucesso.')


            } catch (error) {
                console.log(error)
                callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
            }
        }
    },

    editarSenha: async function (id, dados, callback, error) {

        try {

            let sqlSenha = 'SELECT senha FROM login WHERE id = ? LIMIT 1'

            const [resultSenha] = await connection.query(sqlSenha, id)

            if (bcrypt.compareSync(dados.senha_atual, resultSenha[0].senha)) {

                let sql = 'UPDATE login SET senha=? WHERE id=?',
                    senhaCrip = bcrypt.hashSync(dados.senha, 10)

                const [result] = await connection.query(sql, [senhaCrip, id])

                callback(null, httpStatus.OK, 'Senha atualizada com sucesso.')
            } else {
                callback(new Error(), httpStatus.UNAUTHORIZED, 'Senha atual incorreta!')
            }

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    }


}

module.exports = HomeService