'use strict'

const connection = require('../database/connection'),
    httpStatus = require('http-status'),
    AgendamentosController = require('../controller/AgendamentosController')


var ConsultasService = {

    cadastrar: async function (dados, callback, error) {

        try {  

            let status = '1';

            await AgendamentosController.mudarStatus(dados.agendamento_id, status)

            let sql = 'INSERT INTO consultas (agendamentos_id, pacientes_id, medicos_id, tipo_consulta, ' +
                'motivo_consulta, descricao, observacoes, data, peso, altura, temperatura, pressao, ' +
                'frequencia_card, frequencia_resp) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)'

            const [result] = await connection.query(sql, [dados.agendamento_id, dados.paciente_id, dados.medico_id,
                dados.tipo_consulta, dados.motivo_consulta, dados.descricao, dados.observacoes, dados.data, dados.peso,
                dados.altura, dados.temperatura, dados.pa, dados.fc, dados.fr])

            callback(null, httpStatus.OK, 'Consulta cadastrada com sucesso.', result.insertId)

        } catch (error) {
            console.log(error)
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }

    },

    listarPorId: async function (id, callback) {

        try {
            let sql = 'SELECT medicos.nome as nome_medico, pacientes.nome as nome_paciente, tipo_consulta, ' +
            'motivo_consulta, descricao, observacoes, data, peso, altura, temperatura, ' +
            'pressao, frequencia_card, frequencia_resp FROM consultas ' +
            'JOIN pacientes ON pacientes_id = pacientes.id ' +
            'JOIN medicos ON medicos_id = medicos.id WHERE consultas.id=? LIMIT 1'

            const [result] = await connection.query(sql, id)

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    ultimas: async function (id, callback) {

        try {
            let sql = 'SELECT id, DATE_FORMAT(data, "%d-%m-%Y") as data, motivo_consulta ' +
                'FROM consultas WHERE pacientes_id=? ORDER BY id DESC LIMIT 3;'

            const [result] = await connection.query(sql, id)

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    },

    historico: async function (id, callback) {

        try {
            let sql = 'SELECT id, DATE_FORMAT(data, "%d-%m-%Y") as data, motivo_consulta ' +
                'FROM consultas WHERE pacientes_id=? ORDER BY id DESC'

            const [result] = await connection.query(sql, id)

            callback(null, result)

        } catch (error) {
            callback(error, httpStatus.INTERNAL_SERVER_ERROR, 'Desculpe-nos! Tente Novamente.')
        }
    }

}

module.exports = ConsultasService