'use strict'

const ConsultasService = require('../services/ConsultasService'),
    httpStatus = require('http-status')

var ConsultasController = {

    cadastrar: function (req, res) {
        ConsultasService.cadastrar(req.body, function (error, status, message, resultId) {
            res.status(status).json({message: message, resultId: resultId})
        })
    },

    listarPorId: function (id, res){
        ConsultasService.listarPorId(id, function(error, consulta){
            res.status(httpStatus.OK).json({ consulta : consulta})
        })
    },

    ultimas: function (id, res){
        ConsultasService.ultimas(id, function(error, consultas){
            res.status(httpStatus.OK).json({ consultas : consultas})
        })
    },

    historico: function (id, res){
        ConsultasService.historico(id, function(error, consultas){
            res.status(httpStatus.OK).json({ consultas : consultas})
        })
    }

}

module.exports = ConsultasController