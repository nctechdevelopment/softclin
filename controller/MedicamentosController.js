'use strict'

const MedicamentoService = require('../services/MedicamentosService'),
    httpStatus = require('http-status')

var MedicamentosController = {

    //Cadastrar
    cadastrar: function (req, res) {
        MedicamentoService.cadastrar(req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    },

    listar: function(res){
        MedicamentoService.listar(function(error, medicamentos){
            res.status(httpStatus.OK).json({medicamentos:medicamentos})
        })
    },

    listarPorId: function (id, res){
        MedicamentoService.listarPorId(id, function(error, medicamento){
            res.status(httpStatus.OK).json({ medicamento : medicamento})
        })
    },

    pesquisar: function (pesquisa, res){
        MedicamentoService.pesquisar(pesquisa, function(error, medicamentos){
            res.status(httpStatus.OK).json({ medicamentos : medicamentos})
        })
    },

    editar: function (id, req, res) {
        MedicamentoService.editar(id, req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    }
}

module.exports = MedicamentosController