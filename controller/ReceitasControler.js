'use strict'

const ReceitasService = require('../services/ReceitasService'),
    httpStatus = require('http-status')

var ReceitasController = {

    cadastrar: function (req, res) {
        ReceitasService.cadastrar(req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    },

    medicamentosReceita: function (id, res){
        ReceitasService.medicamentosReceita(id, function(error, medicamentos){
            res.status(httpStatus.OK).json({ medicamentos : medicamentos})
        })
    },

}

module.exports = ReceitasController