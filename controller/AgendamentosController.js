'use strict'

const AgendamentosService = require('../services/AgendamentosService'),
    httpStatus = require('http-status')

var AgendamentosController = {

    cadastrar: function (req, res) {
        AgendamentosService.cadastrar(req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    },

    listarPorMedico: function (id, res){
        AgendamentosService.listarPorMedico(id, function(error, agendamentos){
            res.status(httpStatus.OK).json({ agendamentos : agendamentos})
        })
    },

    retornarAgendamento: function (id_agend, id_med, res){
        AgendamentosService.retornarAgendamento(id_agend, id_med, function(error, agendamento){
            res.status(httpStatus.OK).json({ agendamento : agendamento})
        })
    },

    editar: function (id, req, res) {
        AgendamentosService.editar(id, req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    },

    excluir: function (id, res) {
        AgendamentosService.excluir(id, function (error, status, message) {
            res.status(status).json({message: message})
        })
    },

    mudarStatus: function(id, novoStatus, res){
        AgendamentosService.mudarStatus(id, novoStatus, function(error, status, message){
            res.status(status).json({message: message})
        })
    }



}

module.exports = AgendamentosController