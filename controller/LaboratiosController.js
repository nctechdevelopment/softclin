'use strict'

const LabService = require('../services/LaboratoriosService'),
    httpStatus = require('http-status')

var LaboratoriosController = {

    //Cadastrar
    cadastrar: function (req, res) {
        LabService.cadastrar(req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    },

    listar: function(res){
        LabService.listar(function(error, laboratorios){
            res.status(httpStatus.OK).json({laboratorios:laboratorios})
        })
    },

    listarPorId: function (id, res){
        LabService.listarPorId(id, function(error, laboratorio){
            res.status(httpStatus.OK).json({ laboratorio : laboratorio})
        })
    },

    editar: function (id, req, res) {
        LabService.editar(id, req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    },

    mudarStatus: function(id, req, res){
        LabService.mudarStatus(id, req.body, function(error, status, message){
            res.status(status).json({message: message})
        })
    }
}

module.exports = LaboratoriosController