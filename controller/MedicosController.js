'use strict'

const MedService = require('../services/MedicosService'),
    httpStatus = require('http-status')

var MedicosController = {

    //Cadastrar
    cadastrar: function (req, res) {
        MedService.cadastrar(req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    },

    listar: function(res){
        MedService.listar(function(error, medicos){
            res.status(httpStatus.OK).json({medicos:medicos})
        })
    },

    listarPorId: function (id, res){
        MedService.listarPorId(id, function(error, medico){
            res.status(httpStatus.OK).json({ medico : medico})
        })
    },

    medicosDisponiveis: function(res){
        MedService.medicosDisp(function(error, medicos){
            res.status(httpStatus.OK).json({medicos:medicos})
        })
    },

    editar: function (id, req, res) {
        MedService.editar(id, req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    },

    mudarStatus: function(id, req, res){
        MedService.mudarStatus(id, req.body, function(error, status, message){
            res.status(status).json({message: message})
        })
    }
}

module.exports = MedicosController