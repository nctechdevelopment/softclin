'use strict'

const RecepService = require('../services/RecepcionistasServices'),
    httpStatus = require('http-status')

var RecepcionistasController = {

    //Cadastrar
    cadastrar: function (req, res) {
        RecepService.cadastrar(req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    },

    listar: function(res){
        RecepService.listar(function(error, recepcionistas){
            res.status(httpStatus.OK).json({recepcionistas:recepcionistas})
        })
    },

    listarPorId: function (id, res){
        RecepService.listarPorId(id, function(error, recepcionista){
            res.status(httpStatus.OK).json({ recepcionista : recepcionista})
        })
    },

    editar: function (id, req, res) {
        RecepService.editar(id, req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    },


    mudarStatus: function(id, req, res){
        RecepService.mudarStatus(id, req.body, function(error, status, message){
            res.status(status).json({message: message})
        })
    }
}

module.exports = RecepcionistasController