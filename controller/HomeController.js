'use strict'

const HomeService = require('../services/HomeService'),
    httpStatus = require('http-status')

var HomeController = {

    //Login
    login: function (txtEmail, txtSenha, callback) {
        HomeService.access(txtEmail, txtSenha, function (error, status, message, user) {
            if (status == httpStatus.OK) callback(null, user, message)
            else if (status == httpStatus.UNAUTHORIZED) callback(null, false, message)
            else callback(error)
        })
    },
    loginExame: function (idExame, txtSenha, callback) {
        HomeService.accessExam(idExame, txtSenha, function (error, status, message, user) {
            if (status == httpStatus.OK) callback(null, user, message)
            else if (status == httpStatus.UNAUTHORIZED) callback(null, false, message)
            else callback(error)
        })
    },
    //Cadastro de Login
    cadastrar:  async function (dados, nivel_acesso, callback){
        await HomeService.cadastrar(dados, nivel_acesso, function(error, status, message, loginID){
            if (status == httpStatus.OK) callback(null, loginID)
            else callback(error, message)
        })
    },

    listar: function(res){
        HomeService.listar(function(error, logins){
            res.status(httpStatus.OK).json({logins:logins})
        })
    },

    listarPorId: function (id, res){
        HomeService.listarPorId(id, function(error, login){
            res.status(httpStatus.OK).json({ login : login})
        })
    },

    editar: function (id, req, res) {
        HomeService.editar(id, req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    },

    editarSenha: function (id, req, res) {
        HomeService.editarSenha(id, req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    }

}

module.exports = HomeController