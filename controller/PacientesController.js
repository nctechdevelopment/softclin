'use strict'

const PacienteService = require('../services/PacientesService'),
    httpStatus = require('http-status')

var PacientesController = {

    //Cadastrar
    cadastrar: function (req, res) {
        PacienteService.cadastrar(req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    },

    listar: function(res){
        PacienteService.listar(function(error, pacientes){
            res.status(httpStatus.OK).json({pacientes:pacientes})
        })
    },

    listarPorId: function (id, res){
        PacienteService.listarPorId(id, function(error, paciente){
            res.status(httpStatus.OK).json({ paciente : paciente})
        })
    },

    editar: function (id, req, res) {
        PacienteService.editar(id, req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    },

    pesquisar: function (pesquisa, res){
        PacienteService.pesquisar(pesquisa, function(error, pacientes){
            res.status(httpStatus.OK).json({ pacientes : pacientes})
        })
    },

    mudarStatus: function(id, req, res){
        PacienteService.mudarStatus(id, req.body, function(error, status, message){
            res.status(status).json({message: message})
        })
    }
}

module.exports = PacientesController