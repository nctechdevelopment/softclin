'use strict'

const ExamesService = require('../services/ExamesService'),
    httpStatus = require('http-status')

var ExamesController = {

    cadastrar: function (req, res) {
        ExamesService.cadastrar(req.body, function (error, status, message) {
            res.status(status).json({message: message})
        })
    },

    examesConsulta: function (id, res){
        ExamesService.examesConsulta(id, function(error, exames){
            res.status(httpStatus.OK).json({ exames : exames})
        })
    },

    downloadExames: function (id_cons, id_pac, res){
        ExamesService.downloadExames(id_cons, id_pac, function(error, exames){
            res.status(httpStatus.OK).json({ exames : exames})
        })
    },

    resultadoExames: function (id_cons, res){
        ExamesService.resultadoExames(id_cons, function(error, exames){
            res.status(httpStatus.OK).json({ exames : exames})
        })
    },

    pesquisar: function (pesquisa, res){
        ExamesService.pesquisar(pesquisa, function(error, exames){
            res.status(httpStatus.OK).json({ exames : exames})
        })
    },

    enviarExames: function(id, path, req, res){
        ExamesService.enviarExames(id, path, req.body ,function(error, status, message) {
            res.status(status).json({message: message})
        })
    }


}

module.exports = ExamesController